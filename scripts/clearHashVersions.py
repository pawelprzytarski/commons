#!/usr/bin/env python3

import os
import re
from datetime import datetime, timedelta

import requests

if __name__ == '__main__':
    gitlabToken = None
    tokenType = "PRIVATE-TOKEN"

    if os.environ.get("GITLAB_TOKEN"):
        gitlabToken = os.environ.get("GITLAB_TOKEN")
    elif os.path.exists(".gitlabtoken"):
        with open('.gitlabtoken', 'r') as file:
            gitlabToken = file.read().replace('\n', '')
    elif os.environ.get("env.CI_JOB_TOKEN"):
        gitlabToken = os.environ.get("env.CI_JOB_TOKEN")
        tokenType = "Job-Token"

    if gitlabToken is None:
        print("No gitlab token detected")
        exit()

    headers = {
        tokenType: gitlabToken
    }
    page = 1
    numberOfRemovedPackages = 0
    createdDateThreshold = (datetime.now() + timedelta(days=-7)).strftime("%Y-%m-%dT%H:%M:%S")
    lastUsedDateThreshold = (datetime.now() + timedelta(days=-30)).strftime("%Y-%m-%dT%H:%M:%S")
    while True:
        r = requests.get(
            "https://gitlab.com/api/v4/projects/33441474/packages?package_type=maven&per_page=100&page=" + str(page),
            headers=headers)

        if r.status_code != 200:
            print("Failed connecting to gilab.com/api/v4/")
            exit()

        hashVersionPattern = re.compile(r"^([0-9]+\.){2,3}[0-9]+-\w+$")

        packages = r.json()
        if packages.__len__() == 0:
            break
        for package in packages:
            if hashVersionPattern.match(package['version']) \
                    and package['created_at'] < createdDateThreshold\
                    and (package['last_downloaded_at'] is None or package['last_downloaded_at'] < lastUsedDateThreshold):
                numberOfRemovedPackages = numberOfRemovedPackages + 1
                print("Removing package: " + package["name"] + ":" + package["version"])
                deleteResponse = requests.delete(package["_links"]['delete_api_path'], headers=headers)
            else:
                print("Keeping package: " + package["name"] + ":" + package["version"])
        page = page + 1

    print("Removed " + str(numberOfRemovedPackages) + " packages")
