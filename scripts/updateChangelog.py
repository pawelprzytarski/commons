#!/usr/bin/env python3

import shutil
import re

class ChangelogUpdater:
    def __init__(self, base_path = './', dry_run = False):
        self.replaced_link_pattern = r'[Unreleased]: \1v{0}..master\n[{0}]: \1v\2..v{0}'
        self.unreleased_link_pattern = r'\[Unreleased]: (http[s]://.*/)v([0-9.\-a-zA-Z]*)\.\.master'
        self.unreleased_changes_section = '## [Unreleased]'
        self.clear_unreleased_section = '## [Unreleased]\n### Added\n\n### Changed\n\n### Removed\n\n## [{}]'
        self.base_path = base_path
        self.dry_run = dry_run

    def update_changelog(self):
        if not self.dry_run:
            shutil.copy(self.base_path + 'CHANGELOG.md', self.base_path + 'CHANGELOG.md.old')
        else:
            print("------------------- Dry run -------------------")
        project_version = self.read_project_version()
        print('Updating changelog for release ' + project_version)

        changelog = self.read_changelog()
        changelog = changelog.replace(self.unreleased_changes_section, self.clear_unreleased_section.format(project_version))
        changelog = re.sub(self.unreleased_link_pattern, self.replaced_link_pattern.format(project_version), changelog)

        if not self.dry_run:
            self.save_changelog(changelog)
        else:
            print(changelog)
        print('Changelog updated')

    def save_changelog(self, changelog):
        changelog_file = open(self.base_path + 'CHANGELOG.md', 'w')
        changelog_file.write(changelog)
        changelog_file.close()

    def read_changelog(self):
        changelog_file = open(self.base_path + 'CHANGELOG.md', 'r')
        changelog = "".join(changelog_file.readlines())
        changelog_file.close()
        return changelog

    def read_project_version(self):
        from xml.etree import ElementTree as elementTree
        ns = "http://maven.apache.org/POM/4.0.0"
        elementTree.register_namespace('', ns)
        tree = elementTree.ElementTree()
        tree.parse(self.base_path + 'pom.xml')
        project_version = tree.getroot().find("{%s}version" % ns).text
        return project_version


if __name__ == '__main__':
    ChangelogUpdater("../", False).update_changelog()