#!/bin/sh

set -e

echo "Install JDK"

apk add -X https://dl-cdn.alpinelinux.org/alpine/v3.16/main -u alpine-keys
apk --update add ruby curl bash
apk --update add openjdk17-jre-headless --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

mkdir -p ~/bin && curl -s https://bitbucket.org/mjensen/mvnvm/raw/master/mvn > ~/bin/mvn && chmod 0755 ~/bin/mvn
export PATH="$PATH:~/bin"

java -version

echo "Generate jacoco reports"
~/bin/mvn $MAVEN_CLI_OPTS test-compile -DskipTests -DskipFuncTests -DskipFrontend jacoco:report@report-ut jacoco:report@report-it jacoco:report-aggregate -PaggregateJacoco -s ci_settings.xml

echo "Find coverage percentage"
awk -F, '{instructions += $4 + $5; covered += $5 } END {print covered, "/", instructions, " instructions covered"; print 100*covered/instructions, "% covered" }' aggregate-reports/target/site/jacoco-aggregate/jacoco.csv

echo "Convert jacoco report to cobertura format"
find . -name java -print0 | xargs -r0 echo /opt/cover2cover.py "aggregate-reports/target/site/jacoco-aggregate/jacoco.xml"

find . -name java -print0 | xargs -r0 python /opt/cover2cover.py "aggregate-reports/target/site/jacoco-aggregate/jacoco.xml" > "cobertura.xml"
python /opt/source2filename.py "cobertura.xml"

echo "Job finished"
if test -f cobertura.xml; then
  exit 0
else
  exit 1
fi;