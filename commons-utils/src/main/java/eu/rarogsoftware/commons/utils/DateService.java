package eu.rarogsoftware.commons.utils;

import java.time.Instant;
import java.util.Date;

/**
 * Provide abstraction for accessing current system time
 */
public class DateService {
    public Date getCurrentDate() {
        return Date.from(getCurrentTime());
    }

    public Instant getCurrentTime() {
        return Instant.now();
    }
}

