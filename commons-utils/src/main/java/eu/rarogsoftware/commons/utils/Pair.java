package eu.rarogsoftware.commons.utils;

import java.util.Objects;

public class Pair<L, R> {
    private final L left;
    private final R right;

    public Pair(L left, R right) {
        Objects.requireNonNull(left, "Element of Pair cannot be null");
        Objects.requireNonNull(right, "Element of Pair cannot be null");
        this.left = left;
        this.right = right;
    }

    public L left(){
        return left;
    }

    public R right(){
        return right;
    }

    public static <L, R> Pair<L, R> of(L left, R right){
        return new Pair<>(left, right);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(left, pair.left) && Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "left=" + left +
                ", right=" + right +
                '}';
    }
}
