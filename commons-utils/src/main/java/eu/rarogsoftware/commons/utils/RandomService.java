package eu.rarogsoftware.commons.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Provide abstraction layer in access to Random generators.
 * It increases code testability
 */
public interface RandomService {
    Random getRandom();
    SecureRandom getSecureRandom() throws NoSuchAlgorithmException;
}
