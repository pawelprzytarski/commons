package eu.rarogsoftware.commons.utils;

import java.time.Instant;

/**
 * Provide default implementation of DateService. Allow setting fixed time for all services
 */
public class SystemClockDateService extends DateService {
    @Override
    public Instant getCurrentTime() {
        return Instant.now();
    }
}
