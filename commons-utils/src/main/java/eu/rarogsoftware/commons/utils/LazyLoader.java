package eu.rarogsoftware.commons.utils;


import java.util.function.Supplier;

public abstract class LazyLoader<U> implements Supplier<U> {
    private U object;

    public U get() {
        synchronized (this) {
            if (object == null) {
                object = create();
            }
            return object;
        }
    }

    public boolean isInitialized() {
        synchronized (this) {
            return object != null;
        }
    }

    public void clear() {
        synchronized (this) {
            object = null;
        }
    }

    protected abstract U create();

    public static <T> LazyLoader<T> lazyLoader(Supplier<T> supplier){
        return new LazyLoader<T>() {
            @Override
            protected T create() {
                return supplier.get();
            }
        };
    }
}
