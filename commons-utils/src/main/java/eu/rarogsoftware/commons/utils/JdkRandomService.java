package eu.rarogsoftware.commons.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class JdkRandomService implements RandomService {
    @Override
    public Random getRandom() {
        return ThreadLocalRandom.current();
    }

    @Override
    public SecureRandom getSecureRandom() throws NoSuchAlgorithmException {
        return SecureRandom.getInstanceStrong();
    }
}
