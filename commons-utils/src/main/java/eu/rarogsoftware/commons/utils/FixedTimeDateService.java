package eu.rarogsoftware.commons.utils;

import java.time.Instant;
import java.util.Optional;

/**
 * Provide testable implementation of DateService. Allow to set fixed time for all services
 */
public class FixedTimeDateService extends DateService {
    private Optional<Instant> fixedTime = Optional.empty();

    @Override
    public Instant getCurrentTime() {
        return fixedTime.orElseGet(Instant::now);
    }

    public void setFixedTime(Instant fixedTime) {
        this.fixedTime = Optional.ofNullable(fixedTime);
    }

    public void clearFixedTime() {
        this.fixedTime = Optional.empty();
    }
}
