package eu.rarogsoftware.commons.database.connection;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class SimpleConnectionFactory implements ConnectionFactory {
    private final DataSource dataSource;

    public SimpleConnectionFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public Connection createConnection() throws DatabaseConnectionException{
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new DatabaseConnectionException(e);
        }
    }
}
