package eu.rarogsoftware.commons.database.connection;

import com.querydsl.sql.SQLQueryFactory;

/**
 * Creates and manages QueryDsl connections
 * Provide abstraction for management of database transactions
 * and for connection pool
 */
public interface DatabaseConnectionProvider {
    /**
     * Database transaction level
     */
    enum TransactionLevel {
        NONE,
        READ_UNCOMMITTED,
        READ_COMMITTED,
        REPEATABLE_READ,
        SERIALIZABLE
    }

    /**
     * Interface for database queries. It allows hiding transaction and connection management
     *
     * @param <U> query return type
     */
    interface DbOperation<U, T extends Throwable> {
        U operation(SQLQueryFactory query) throws T;
    }

    /**
     * @return connection factory used to create connections
     */
    ConnectionFactory getConnectionFactory();

    /**
     * If provide use connection pool then this methods wait for all connections to end and then close connections
     * If connections didn't end before timeout then force close all connections to prevent deadlock
     *
     * @throws DatabaseConnectionException when there is problem with SQL statement or connection
     */
    void cleanupConnections();

    /**
     * Set timeout for acquiring free connections from connection pool. It also defines timeout for {@link DatabaseConnectionProvider#cleanupConnections()}
     * @param value
     */
    void setTimeoutMilliseconds(int value);

    /**
     * Acquire free connection and wraps it in QueryDsl
     * This method use default transaction level and autocommit
     * In case of exception transaction does not rollback
     * @param operation operation/query to be executed
     * @param <U> return type of operation
     * @return result of provided operation
     * @throws DatabaseConnectionException in case of problems with acquiring or freeing connection
     */
    <U, T extends Throwable> U getConnection(DbOperation<U, T> operation) throws T;

    /**
     * Acquire free connection and wraps it in QueryDsl
     * This method use specified transaction level and commit changes after operation closes
     * In case of exception transaction automatically rollbacks
     * @param operation operation/query to be executed
     * @param <U> return type of operation
     * @return result of provided operation
     * @throws DatabaseConnectionException in case of problems with acquiring or freeing connection
     */
    <U, T extends Throwable> U getConnectionInTransaction(TransactionLevel transactionLevel, DbOperation<U, T> operation) throws T;
}
