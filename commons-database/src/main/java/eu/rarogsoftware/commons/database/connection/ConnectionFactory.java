package eu.rarogsoftware.commons.database.connection;

import java.sql.Connection;

public interface ConnectionFactory {
    Connection createConnection() throws DatabaseConnectionException;
}
