package eu.rarogsoftware.commons.database.connection;

import com.querydsl.sql.H2Templates;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;

import java.sql.Connection;
import java.util.function.Supplier;

public class H2DatabaseConnectionProvider extends AbstractDatabaseConnectionProvider {

    public H2DatabaseConnectionProvider(ConnectionFactory connectionFactory, boolean reusableConnections) {
        this(connectionFactory, reusableConnections ? 20 : 0);
    }

    public H2DatabaseConnectionProvider(ConnectionFactory connectionFactory, int numberOfConnections) {
        super(connectionFactory, numberOfConnections);
    }

    @Override
    protected SQLQueryFactory createSqlFactory(Supplier<Connection> connection) {
        SQLTemplates templates = new H2Templates();
        return new SQLQueryFactory(templates, connection);
    }
}
