package eu.rarogsoftware.commons.database.connection;

public class RecurrentConnectionException extends DatabaseConnectionException {
    public RecurrentConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecurrentConnectionException(Throwable cause) {
        super(cause);
    }

    public RecurrentConnectionException(String message) {
        super(message);
    }
}
