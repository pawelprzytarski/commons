package eu.rarogsoftware.commons.database.connection;

import com.querydsl.sql.SQLQueryFactory;
import eu.rarogsoftware.commons.utils.LazyLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public abstract class AbstractDatabaseConnectionProvider implements DatabaseConnectionProvider {
    private final Logger log = LoggerFactory.getLogger(AbstractDatabaseConnectionProvider.class);
    protected static ThreadLocal<Boolean> connectionActive = ThreadLocal.withInitial(() -> false);
    protected final ConnectionFactory connectionFactory;
    protected Queue<LazyLoader<Connection>> connectionQueue;
    protected List<LazyLoader<Connection>> connections;
    protected Semaphore semaphore;
    protected int timeoutMilliseconds = 10000;

    public AbstractDatabaseConnectionProvider(ConnectionFactory connectionFactory, int numberOfConnections) {
        this.connectionFactory = connectionFactory;
        populateConnectionQueue(numberOfConnections);
    }

    private void populateConnectionQueue(int numberOfConnections) {
        if (numberOfConnections > 0) {
            log.info("Creating connection poll with size: {}", numberOfConnections);
            semaphore = new Semaphore(numberOfConnections);
            connectionQueue = new ConcurrentLinkedDeque<>();
            connections = new ArrayList<>();
            for (int i = 0; i < numberOfConnections; i++) {
                LazyLoader<Connection> connection = createConnection();
                connectionQueue.add(connection);
                connections.add(connection);
            }
        } else {
            log.info("Enabled creating connection on request");
            connectionQueue = null;
        }
    }

    public void setTimeoutMilliseconds(int value) {
        this.timeoutMilliseconds = value;
        log.info("Changed timeout for connections. New value: {}ms", value);
    }

    @Override
    public void cleanupConnections() {
        if (connectionQueue != null) {
            log.info("Cleaning up connection poll");
            try {
                if (!semaphore.tryAcquire(connections.size(), timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
                    log.warn("Waiting for connections to free timeout. Force closing connections");
                } else {
                    log.info("Cleaning up connection normally");
                }
            } catch (InterruptedException e) {
                log.warn("Caught interrupted. Force closing connections ", e);
            }
            semaphore.drainPermits();
            for (LazyLoader<Connection> lazyLoader : connections) {
                try {
                    if (lazyLoader.isInitialized() && !lazyLoader.get().isClosed()) {
                        lazyLoader.get().close();
                        lazyLoader.clear();
                    }
                } catch (SQLException e) {
                    throw new DatabaseConnectionException(e);
                }
            }
            semaphore.release(connections.size());
        }
    }

    protected LazyLoader<Connection> createConnection() {
        return new LazyLoader<>() {
            @Override
            protected Connection create() {
                return connectionFactory.createConnection();
            }
        };
    }

    @Override
    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    @Override
    public <U, T extends Throwable> U getConnection(DbOperation<U, T> operation) throws T {
        log.trace("Using connection without transaction");
        if (connectionActive.get()) {
            var exception = new RecurrentConnectionException("Recurrent connection detected");
            log.warn("Attempt to start database connection inside already active connection!", exception);
            throw exception;
        }
        LazyLoader<Connection> connection = retrieveConnection();
        U result;
        try {
            connectionActive.set(true);
            result = operation.operation(createSqlFactory(connection));
        } finally {
            connectionActive.set(false);
            cleanUpConnection(connection);
            log.trace("Freed connection without transaction");
        }
        return result;
    }

    protected LazyLoader<Connection> retrieveConnection() {
        LazyLoader<Connection> connection;
        if (connectionQueue != null) {
            try {
                if (!semaphore.tryAcquire(timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
                    throw new DatabaseConnectionException("Connection pool full. Timeout when waiting for free connection");
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            connection = connectionQueue.poll();
        } else {
            connection = createConnection();
        }
        if (connection == null) {
            log.debug("Creating new connection");
            connection = createConnection();
        }
        return connection;
    }

    protected void cleanUpConnection(LazyLoader<Connection> connection) {
        if (connectionQueue != null) {
            try {
                if (connection != null && connection.get().isClosed()) {
                    connection = null;
                }
            } catch (SQLException e) {
                connection = null;
            }
            if (connection == null) {
                log.warn("Connection from connection pool closed by consumer. Recreating connection");
                connection = createConnection();
            }
            connectionQueue.add(connection);
            semaphore.release();
        } else {
            try {
                log.debug("Closing connection after use");
                if (connection != null && !connection.get().isClosed()) {
                    connection.get().close();
                }
            } catch (SQLException e) {
                throw new DatabaseConnectionException(e);
            }
        }
    }

    protected abstract SQLQueryFactory createSqlFactory(Supplier<Connection> connection);

    @Override
    public <U, T extends Throwable> U getConnectionInTransaction(TransactionLevel transactionLevel, DbOperation<U, T> operation) throws T {
        LazyLoader<Connection> connection = retrieveConnection();
        try {
            log.trace("Using connection with transaction level: {}", transactionLevel);
            connection.get().setAutoCommit(false);
            connection.get().setTransactionIsolation(translateTransactionLevel(transactionLevel));
            U result;
            try {
                result = operation.operation(createSqlFactory(connection));
                connection.get().commit();
            } catch (Throwable e) {
                connection.get().rollback();
                throw e;
            } finally {
                connection.get().setAutoCommit(true);
                connection.get().setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                cleanUpConnection(connection);
                log.trace("Freed connection with transaction level: {}", transactionLevel);
            }
            return result;
        } catch (SQLException e) {
            cleanUpConnection(null);
            throw new DatabaseConnectionException(e);
        }
    }

    private int translateTransactionLevel(TransactionLevel transactionLevel) {
        return switch (transactionLevel) {
            case NONE -> Connection.TRANSACTION_NONE;
            case READ_UNCOMMITTED -> Connection.TRANSACTION_READ_UNCOMMITTED;
            case READ_COMMITTED -> Connection.TRANSACTION_READ_COMMITTED;
            case REPEATABLE_READ -> Connection.TRANSACTION_REPEATABLE_READ;
            case SERIALIZABLE -> Connection.TRANSACTION_SERIALIZABLE;
        };
    }
}
