package eu.rarogsoftware.commons.database.connection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DatabaseConnectionProviderTest {
    @Mock
    ConnectionFactory connectionFactory;
    @Mock
    Connection connection;

    DatabaseConnectionProvider connectionProvider;

    @Test
    void getConnection_dontUseReusable_getTemporaryConnection() throws Exception {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, false);

        boolean result = connectionProvider.getConnection(Objects::nonNull);

        assertTrue(result);
        verify(connectionFactory, times(1)).createConnection();
        verify(connection, times(1)).close();
    }

    @Test
    void getConnection_useReusable_getReusableConnection() throws Exception {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, true);

        boolean result = connectionProvider.getConnection(Objects::nonNull);

        assertTrue(result);
        verify(connectionFactory, times(1)).createConnection();
        verify(connection, times(0)).close();
    }

    @Test
    void getConnection_dontUseReusableInvokeMultipleTimes_useDifferentConnections() throws Exception {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, false);

        connectionProvider.getConnection(Objects::nonNull);
        connectionProvider.getConnection(Objects::nonNull);

        verify(connectionFactory, times(2)).createConnection();
        verify(connection, times(2)).close();
    }

    @Test
    void getConnection_dontUseReusableAndConnectionAlreadyClosed_dontCloseConnectionSecondTime() throws Exception {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(true);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, false);

        connectionProvider.getConnection(Objects::nonNull);

        verify(connection, times(0)).close();
    }

    @Test
    void getConnection_reusableAndConnectionAlreadyClosed_recreateConnection() throws Exception {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(true);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, 1);

        connectionProvider.getConnection(Objects::nonNull);
        connectionProvider.getConnection(Objects::nonNull);

        verify(connectionFactory, times(2)).createConnection();
    }

    @Test
    void getConnection_reusableAndMultipleConnections_createConnectionsToLimit() throws SQLException, InterruptedException {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, 2);
        Lock lock = new ReentrantLock();
        lock.lock();
        ExecutorService executorService = new ForkJoinPool(10);
        IntStream.rangeClosed(0, 10).<Callable<Integer>>mapToObj(i -> () -> connectionProvider.getConnection((factory) -> {
            lock.lock();
            lock.unlock();
            return 1;
        })).forEach(executorService::submit);
        lock.unlock();
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        verify(connectionFactory, times(2)).createConnection();
        verify(connection, times(0)).close();
    }

    @Test
    void cleanUpOpenedConnections_reusableAndMultipleConnections_closeConnections() throws SQLException, InterruptedException {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, 2);
        Lock lock = new ReentrantLock();
        lock.lock();
        ExecutorService executorService = new ForkJoinPool(10);
        IntStream.rangeClosed(0, 10).<Callable<Integer>>mapToObj(i -> () -> connectionProvider.getConnection((factory) -> {
            lock.lock();
            lock.unlock();
            return 1;
        })).forEach(executorService::submit);
        lock.unlock();
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        connectionProvider.cleanupConnections();

        verify(connectionFactory, times(2)).createConnection();
        verify(connection, times(2)).close();
    }

    @Test
    void cleanUpOpenedConnections_connectionsBlocked_forceCloseConnections() throws SQLException, InterruptedException {
        when(connectionFactory.createConnection()).thenReturn(connection);
        when(connection.isClosed()).thenReturn(false);
        connectionProvider = new H2DatabaseConnectionProvider(connectionFactory, 2);
        connectionProvider.setTimeoutMilliseconds(100);
        Lock lock = new ReentrantLock();
        lock.lock();
        ExecutorService executorService = new ForkJoinPool(10);
        IntStream.rangeClosed(0, 10).<Callable<Integer>>mapToObj(i -> () -> connectionProvider.getConnection((factory) -> 1)).forEach(executorService::submit);
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
        IntStream.rangeClosed(0, 10).<Callable<Integer>>mapToObj(i -> () -> connectionProvider.getConnection((factory) -> {
            lock.lock();
            lock.unlock();
            return 1;
        })).forEach(executorService::submit);

        executorService.awaitTermination(10, TimeUnit.MILLISECONDS);
        connectionProvider.cleanupConnections();

        verify(connectionFactory, times(2)).createConnection();
        verify(connection, times(2)).close();
        lock.unlock();
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
    }
}