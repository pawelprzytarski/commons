package eu.rarogsoftware.commons.cache;

/**
 * Provides abstraction layer for accessing CacheManagers
 */
public interface CacheService {
    /**
     * Get guava like API for creating JCache
     * @param <K> Key type
     * @param <V> Value type
     * @return Cache instance
     */
    <K, V> CacheBuilder<K, V> getBuilder();

    /**
     *
     * @return JCache cache manager or null if JCache not configured
     */
    javax.cache.CacheManager getJCache();

    /**
     * Force clear all caches
     */
    void clearAllCaches();

}
