package eu.rarogsoftware.commons.cache;

import javax.cache.Cache;
import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheWriter;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static javax.cache.expiry.Duration.ETERNAL;

public abstract class AbstractCacheBuilder<K, V> implements CacheBuilder<K, V> {
    protected String name;
    protected javax.cache.expiry.Duration expirationForCreation = ETERNAL;
    protected javax.cache.expiry.Duration expirationForAccess = ETERNAL;
    protected javax.cache.expiry.Duration expirationForUpdate = ETERNAL;
    protected CacheLoader<K, V> cacheLoader;
    protected CacheWriter<K, V> cacheWriter;
    protected boolean statisticsEnabled = false;
    protected boolean managementEnabled = false;
    protected boolean storeByValue = true;

    @Override
    public CacheBuilder<K, V> name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public CacheBuilder<K, V> eternal() {
        expirationForCreation = ETERNAL;
        expirationForAccess = ETERNAL;
        expirationForUpdate = ETERNAL;
        return this;
    }

    @Override
    public CacheBuilder<K, V> expireAfterCreate(Duration duration) {
        return expireAfterCreate(duration.toMillis(), TimeUnit.NANOSECONDS);
    }

    @Override
    public CacheBuilder<K, V> expireAfterCreate(long duration, TimeUnit unit) {
        expirationForCreation = new javax.cache.expiry.Duration(unit, duration);
        return this;
    }

    @Override
    public CacheBuilder<K, V> expireAfterWrite(Duration duration) {
        return expireAfterWrite(duration.toMillis(), TimeUnit.NANOSECONDS);
    }

    @Override
    public CacheBuilder<K, V> expireAfterWrite(long duration, TimeUnit unit) {
        expirationForCreation = new javax.cache.expiry.Duration(unit, duration);
        expirationForUpdate = expirationForCreation;
        return this;
    }

    @Override
    public CacheBuilder<K, V> expireAfterAccess(Duration duration) {
        return expireAfterAccess(duration.toMillis(), TimeUnit.NANOSECONDS);
    }

    @Override
    public CacheBuilder<K, V> expireAfterAccess(long duration, TimeUnit unit) {
        expirationForCreation = new javax.cache.expiry.Duration(unit, duration);
        expirationForAccess = expirationForCreation;
        return this;
    }

    @Override
    public CacheBuilder<K, V> expireAfterTouched(Duration duration) {
        return expireAfterTouched(duration.toMillis(), TimeUnit.NANOSECONDS);
    }

    @Override
    public CacheBuilder<K, V> expireAfterTouched(long duration, TimeUnit unit) {
        expirationForCreation = new javax.cache.expiry.Duration(unit, duration);
        expirationForAccess = expirationForCreation;
        expirationForUpdate = expirationForCreation;
        return this;
    }

    @Override
    public CacheBuilder<K, V> cacheLoader(CacheLoader<K, V> cacheLoader) {
        this.cacheLoader = cacheLoader;
        return this;
    }

    @Override
    public CacheBuilder<K, V> cacheWriter(CacheWriter<K, V> cacheWriter) {
        this.cacheWriter = cacheWriter;
        return this;
    }

    @Override
    public CacheBuilder<K, V> statisticsEnabled(boolean statisticsEnabled) {
        this.statisticsEnabled = statisticsEnabled;
        return this;
    }

    @Override
    public CacheBuilder<K, V> managementEnabled(boolean managementEnabled) {
        this.managementEnabled = managementEnabled;
        return this;
    }

    @Override
    public CacheBuilder<K, V> storeByValue(boolean storeByValue) {
        this.storeByValue = storeByValue;
        return this;
    }

    @Override
    public abstract Cache<K, V> build();
}
