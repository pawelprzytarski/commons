package eu.rarogsoftware.commons.cache;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.configuration.*;
import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CompletionListener;
import javax.cache.processor.EntryProcessor;
import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.EntryProcessorResult;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * Mock implementation of cache used in tests
 * It uses {@link Map} underneath
 * @param <K>
 * @param <V>
 */
public class MockCacheBuilder<K, V> extends AbstractCacheBuilder<K, V> {
    @Override
    public Cache<K, V> build() {
        MutableConfiguration<K, V> configuration = new MutableConfiguration<K, V>();
        if (cacheLoader != null) {
            configuration.setCacheLoaderFactory(new FactoryBuilder.SingletonFactory<>(cacheLoader));
            configuration.setReadThrough(true);
        }
        if (cacheWriter != null) {
            configuration.setCacheWriterFactory(new FactoryBuilder.SingletonFactory<>(cacheWriter));
            configuration.setWriteThrough(true);
        }
        configuration.setStatisticsEnabled(statisticsEnabled);
        configuration.setManagementEnabled(managementEnabled);
        configuration.setStoreByValue(storeByValue);
        return new MockCache<>(configuration);
    }
}

class MockCache<K, V> implements Cache<K, V> {
    private final Map<K, V> cacheMap = new ConcurrentHashMap<>();
    private final CacheLoader<K, V> cacheLoader;
    private final CompleteConfiguration<K, V> configuration;

    MockCache(CompleteConfiguration<K, V> configuration) {
        this.configuration = configuration;
        CacheLoader<K, V> cacheLoader;
        if (configuration.getCacheLoaderFactory() == null) {
            cacheLoader = new AbstractCacheLoader<K, V>() {
                @Override
                public V load(K key) throws CacheLoaderException {
                    return null;
                }
            };
        } else {
            cacheLoader = configuration.getCacheLoaderFactory().create();
        }
        this.cacheLoader = cacheLoader;
    }

    @Override
    public V get(K key) {
        V value = cacheMap.get(key);
        if (value == null) {
            return cacheLoader.load(key);
        }
        return value;
    }

    @Override
    public Map<K, V> getAll(Set<? extends K> keys) {
        return keys.stream().collect(Collectors.toMap(key -> key, this::get));
    }

    @Override
    public boolean containsKey(K key) {
        return cacheMap.containsKey(key);
    }

    @Override
    public void loadAll(Set<? extends K> keys, boolean replaceExistingValues, CompletionListener completionListener) {
        Map<K, V> kvMap = cacheLoader.loadAll(keys);
        if (replaceExistingValues) {
            cacheMap.putAll(kvMap);
        } else {
            kvMap.forEach(cacheMap::putIfAbsent);
        }
        completionListener.onCompletion();
    }

    @Override
    public void put(K key, V value) {
        cacheMap.put(key, value);
    }

    @Override
    public V getAndPut(K key, V value) {
        V v = cacheMap.get(key);
        cacheMap.put(key, value);
        return v;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        cacheMap.putAll(map);
    }

    @Override
    public boolean putIfAbsent(K key, V value) {
        return false;
    }

    @Override
    public boolean remove(K key) {
        return cacheMap.remove(key) != null;
    }

    @Override
    public boolean remove(K key, V oldValue) {
        return cacheMap.remove(key, oldValue);
    }

    @Override
    public V getAndRemove(K key) {
        return cacheMap.remove(key);
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        return cacheMap.replace(key, oldValue, newValue);
    }

    @Override
    public boolean replace(K key, V value) {
        return cacheMap.replace(key, value) != null;
    }

    @Override
    public V getAndReplace(K key, V value) {
        return cacheMap.replace(key, value);
    }

    @Override
    public void removeAll(Set<? extends K> keys) {
        keys.forEach(cacheMap::remove);
    }

    @Override
    public void removeAll() {
        cacheMap.clear();
    }

    @Override
    public void clear() {
        cacheMap.clear();
    }

    @Override
    public <C extends Configuration<K, V>> C getConfiguration(Class<C> clazz) {
        if (clazz.isAssignableFrom(configuration.getClass())) {
            throw new IllegalArgumentException();
        }
        return (C) configuration;
    }

    @Override
    public <T> T invoke(K key, EntryProcessor<K, V, T> entryProcessor, Object... arguments) throws EntryProcessorException {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> Map<K, EntryProcessorResult<T>> invokeAll(Set<? extends K> keys, EntryProcessor<K, V, T> entryProcessor, Object... arguments) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public CacheManager getCacheManager() {
        return null;
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isClosed() {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> clazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void registerCacheEntryListener(CacheEntryListenerConfiguration<K, V> cacheEntryListenerConfiguration) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deregisterCacheEntryListener(CacheEntryListenerConfiguration<K, V> cacheEntryListenerConfiguration) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return cacheMap.entrySet().stream().map(kvEntry -> (Cache.Entry<K, V>) new Cache.Entry<K, V>() {
            @Override
            public K getKey() {
                return null;
            }

            @Override
            public V getValue() {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> clazz) {
                throw new UnsupportedOperationException();
            }
        })
                .iterator();
    }
}