package eu.rarogsoftware.commons.cache;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.configuration.FactoryBuilder;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.ExpiryPolicy;
import java.io.Serializable;

public class DefaultCacheBuilder<K, V> extends AbstractCacheBuilder<K, V> {
    private final CacheManager cacheManager;

    DefaultCacheBuilder(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    CacheBuilder<K, V> newBuilder(CacheManager cacheManager) {
        return new DefaultCacheBuilder<>(cacheManager);
    }

    @Override
    public Cache<K, V> build() {
        Cache<K, V> existingCache = cacheManager.getCache(name);
        if (existingCache != null) {
            return existingCache;
        }
        MutableConfiguration<K, V> configuration = new MutableConfiguration<K, V>()
                .setExpiryPolicyFactory(new FactoryBuilder.SingletonFactory<>(new AllExpiryPolicy(expirationForCreation, expirationForAccess, expirationForUpdate)));
        if (cacheLoader != null) {
            configuration.setCacheLoaderFactory(new FactoryBuilder.SingletonFactory<>(cacheLoader));
            configuration.setReadThrough(true);
        }
        if (cacheWriter != null) {
            configuration.setCacheWriterFactory(new FactoryBuilder.SingletonFactory<>(cacheWriter));
            configuration.setWriteThrough(true);
        }
        configuration.setStatisticsEnabled(statisticsEnabled);
        configuration.setManagementEnabled(managementEnabled);
        configuration.setStoreByValue(storeByValue);
        return cacheManager.createCache(name, configuration);
    }

    static class AllExpiryPolicy implements ExpiryPolicy, Serializable {
        private final Duration expirationForCreation;
        private final Duration expirationForAccess;
        private final Duration expirationForUpdate;

        AllExpiryPolicy(Duration expirationForCreation, Duration expirationForAccess, Duration expirationForUpdate) {
            this.expirationForCreation = expirationForCreation;
            this.expirationForAccess = expirationForAccess;
            this.expirationForUpdate = expirationForUpdate;
        }

        @Override
        public javax.cache.expiry.Duration getExpiryForCreation() {
            return expirationForCreation;
        }

        @Override
        public javax.cache.expiry.Duration getExpiryForAccess() {
            return expirationForAccess;
        }

        @Override
        public javax.cache.expiry.Duration getExpiryForUpdate() {
            return expirationForUpdate;
        }
    }
}
