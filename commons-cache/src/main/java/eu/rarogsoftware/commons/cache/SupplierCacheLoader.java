package eu.rarogsoftware.commons.cache;

import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheLoaderException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class SupplierCacheLoader<K, V> implements CacheLoader<K, V>, Serializable {
    private final transient Function<K, V> supplier;

    public SupplierCacheLoader(Function<K, V> supplier) {
        this.supplier = supplier;
    }

    @Override
    public V load(K key) throws CacheLoaderException {
        return supplier.apply(key);
    }

    @Override
    public Map<K, V> loadAll(Iterable<? extends K> keys) throws CacheLoaderException {
        Map<K, V> beanMap = new HashMap<>();
        keys.forEach(key -> beanMap.put(key, load(key)));
        return Collections.unmodifiableMap(beanMap);
    }
}
