package eu.rarogsoftware.commons.cache;

import javax.cache.Cache;
import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheWriter;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Guava-like wrapper for JCache {@link javax.cache.CacheManager}
 * It retrieve cache if it exists or create new one
 * @param <K> keys type
 * @param <V> values type
 */
public interface CacheBuilder<K, V> {
    /**
     * name of cache - caches with the same name are treated as the same cache
     */
    CacheBuilder<K, V> name(String name);

    /**
     * Configure cache to never expire
     */
    CacheBuilder<K, V> eternal();

    /**
     * Cache values will expire after putting them into cache.
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterCreate(Duration duration);

    /**
     * Cache values will expire after putting or updating them into cache.
     * @param duration lifetime of cache value in specified time unit
     * @param unit time unit of duration
     */
    CacheBuilder<K, V> expireAfterCreate(long duration, TimeUnit unit);

    /**
     * Cache values will expire after putting or updating them into cache.
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterWrite(Duration duration);

    CacheBuilder<K, V> expireAfterWrite(long duration, TimeUnit unit);

    /**
     * Cache values will expire specified time after last read
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterAccess(Duration duration);

    /**
     * Cache values will expire specified time after last read
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterAccess(long duration, TimeUnit unit);

    /**
     * Cache values will expire specified time after last operation
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterTouched(Duration duration);

    /**
     * Cache values will expire specified time after last operation
     * @param duration lifetime of cached value
     */
    CacheBuilder<K, V> expireAfterTouched(long duration, TimeUnit unit);

    /**
     * Sets cache to read-through mode. It mean if cache didn't find key
     * then it will ask cacheLoader to create new cache value for this key
     * @param cacheLoader
     */
    CacheBuilder<K, V> cacheLoader(CacheLoader<K, V> cacheLoader);

    CacheBuilder<K, V> cacheWriter(CacheWriter<K, V> cacheWriter);

    /**
     * Enables/disables cache statistics collection. Default disabled
     * @param statisticsEnabled
     */
    CacheBuilder<K, V> statisticsEnabled(boolean statisticsEnabled);

    /**
     * Enables/disables cache management. Default disabled
     * @param managementEnabled
     */
    CacheBuilder<K, V> managementEnabled(boolean managementEnabled);

    /**
     * If true then cache serialize objects. If false then cache keeps only references to objects
     * Default: true
     * @param storeByValue
     */
    CacheBuilder<K, V> storeByValue(boolean storeByValue);

    /**
     * Retrieve existing instance of cache or create new cache with specified configuration
     * @return configured cache
     */
    Cache<K, V> build();
}
