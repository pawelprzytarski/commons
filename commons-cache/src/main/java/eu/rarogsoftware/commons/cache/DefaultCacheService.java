package eu.rarogsoftware.commons.cache;


import javax.cache.CacheManager;

public class DefaultCacheService implements CacheService {
    private final CacheManager jCache;

    public DefaultCacheService(CacheManager jCache) {
        this.jCache = jCache;
    }

    @Override
    public <K, V> CacheBuilder<K, V> getBuilder() {
        return new DefaultCacheBuilder<K, V>(getJCache());
    }

    @Override
    public CacheManager getJCache() {
        return jCache;
    }

    @Override
    public void clearAllCaches() {
        CacheManager jCache = getJCache();
        jCache.getCacheNames().forEach(cacheName -> jCache.getCache(cacheName).clear());
    }
}

