package eu.rarogsoftware.commons.cache;

import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheLoaderException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCacheLoader<K, V> implements CacheLoader<K, V> {
    @Override
    public Map<K, V> loadAll(Iterable<? extends K> keys) throws CacheLoaderException {
        Map<K, V> beanMap = new HashMap<>();
        keys.forEach(key -> beanMap.put(key, load(key)));
        return Collections.unmodifiableMap(beanMap);
    }
}

