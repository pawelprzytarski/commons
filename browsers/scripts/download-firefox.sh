#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/commons.sh"

os=$1
version=$2
name=firefox-${version}-$os

case "$os" in
linux64)
  file=${name}.tar.bz2
  url="https://ftp.mozilla.org/pub/firefox/releases/${version}/linux-x86_64/en-US/firefox-${version}.tar.bz2"
  ;;
windows)
  file=${name}.exe
  url="https://ftp.mozilla.org/pub/firefox/releases/${version}/win32/en-US/Firefox%20Setup%20${version}.exe"
  ;;
*)
  fail "Invalid operating system ${os}"
  ;;
esac

getFile "${file}" "${url}" || fail "Failed to download firefox from ${url}"

case "$os" in
linux | linux64)
  unpackTarBz2 "$name"
  cd firefox
  echo "Firefox-${version}-${os}" >browser.package
  zipFile "../$name" "*"
  cd ..
  rm -rf firefox
  ;;
windows)
  unpack7zip "$name"
  cd "$name/core"
  echo "Firefox-${version}-${os}" >browser.package
  zipFile "../../$name" "*"
  cd ../..
  rm -rf $name
  ;;
*)
  fail "Unknown Operating System: ${1}"
  ;;
esac

echo "Browser has been repackaged: ${name}.zip"
