
set -euo pipefail

cd $(dirname "$0")

imageName="registry.gitlab.com/rarogsoftware/commons/webdriver-runner"

docker build -t "${imageName}" .
docker tag "${imageName}" "${imageName}:latest"
docker push "${imageName}:latest"

if [[ -n "$CI_SPECIAL_TAG" ]]; then
  docker tag "${imageName}" "${imageName}:$CI_SPECIAL_TAG"
  docker push "${imageName}:$CI_SPECIAL_TAG"
fi
