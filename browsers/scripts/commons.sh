#!/bin/bash

set -euo pipefail

MAVEN_REPO_URL=https://gitlab.com/api/v4/projects/33048100/packages/maven
MAVEN_REPO_ID=gitlab-maven

fail() {
  echo "$1"
  exit 1
}

command_available() {
  type "$1" &>/dev/null
}

getFile() {
  local output="$1"
  local url="$2"

  echo "Downloading ${url} to ${output}"

  if command_available curl; then
    curl -s -L -o "$output" "$url"
  else
    wget --quiet -O "$output" "$url"
  fi
  return $?
}

unpackTarBz2() {
  local name=$1
  echo "Unpacking ${name}.tar.bz2"
  bunzip2 "${name}.tar.bz2"
  tar -xf "${name}.tar"
  rm "${name}".tar
}

unpack7zip() {
  local name=$1
  local filename
  if [ -z "${2:-}" ]; then
    filename="${name}.exe"
  else
    filename="$2"
  fi
  local impostorFileName="${name}.7z"

  if command_available 7z; then
    commandName="7z"
  elif command_available p7zip; then
    commandName="p7zip"
  else
    fail "Unable to extract ${name}.exe\n Missing 7z extractor"
  fi

  mkdir "$name"
  cd "$name"

  if [ $commandName = "7z" ]; then
    echo "Unpacking $filename"
    errors="$($commandName x -y "../$filename" 2>&1)" || fail "Failed to extract: $filename: $errors"
  else
    mv "$filename" "$impostorFileName"
    echo "Unpacking $impostorFileName"
    errors="$($commandName -d "../$impostorFileName" 2>&1)" || fail "Failed to extract: $impostorFileName: $errors"
  fi

  cd ..
  rm -f "$filename"
  rm -f "$impostorFileName"
}

zipFile() {
  local name=$1
  local path=$2
  if command_available zip; then
    zip -q -r "${name}.zip" ${path}
  else
    echo "ERROR: zip command not found."
    exit 1
  fi
}

jarFile() {
  local name=$1
  local path=$2
  if command_available jar; then
    jar Mcf "${name}.jar" ${path}
  else
    echo "ERROR: jar command not found."
    exit 1
  fi
}

artifact_exists() {
  local group="$1"
  local artifact="$2"
  local version="$3"
  local classifier="${4-}"
  local extension="${5:-jar}"

  local resource="$(tr . / <<<"$group")/$artifact/$version/$artifact-$version${classifier:+"-$classifier"}.$extension"
  local output

  if ! output="$(mvn -e -N org.codehaus.mojo:wagon-maven-plugin:2.0.0:exist -f "$(dirname "$0")/../pom.xml" -Dwagon.serverId="${MAVEN_REPO_ID}" -Dwagon.url="${MAVEN_REPO_URL}" -Dwagon.resource="$resource")"; then
    echo "!!! Failed to execute maven command:"
    echo "$output"
    exit 1
  fi

  if grep "$resource exists." <<<"$output" >/dev/null; then
    echo "$resource already exists"
  else
    echo "$resource does not exist"
    return 1
  fi
}