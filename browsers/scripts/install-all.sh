#!/usr/bin/env bash

set -euo pipefail

get_mvn_prop() {
  mvn -N -f "$(dirname "$0")/../pom.xml" -q exec:exec -Dexec.executable=echo -Dexec.args="\${$@}"
}

directory="$(dirname "$0")"

deploy="false"

if [[ "${1:-}" = "--deploy" ]]; then
  deploy="true"
fi

firefoxVersion="$(get_mvn_prop firefox.version)"
${directory}/install-browser.sh firefox windows ${firefoxVersion} $deploy
${directory}/install-browser.sh firefox linux64 ${firefoxVersion} $deploy

firefoxProfileVersion="$(get_mvn_prop firefox.profile.version)"
geckodriverVersion="$(get_mvn_prop geckodriver.version)"
${directory}/install-firefox-profile.sh ${geckodriverVersion} ${firefoxProfileVersion} windows $deploy
${directory}/install-firefox-profile.sh ${geckodriverVersion} ${firefoxProfileVersion} linux64 $deploy

