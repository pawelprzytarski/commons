#!/bin/bash

set -euo pipefail

browser=$1
os=$2
version=$3
deploy=$4

path=$(pwd)"/"$(dirname "$0")

tmpdir=$(mktemp -d)

source "$(dirname "$0")/commons.sh"

if command_available mvn; then
  echo ""
else
  echo "ERROR: mvn command not found"
  exit 1
fi

group="eu.rarogsoftware.browsers"
artifact=${browser}
artifactVersion=${version}
classifier=${os}
extension="jar"

if [ "$deploy" == "true" ]; then
  if artifact_exists "$group" "$artifact" "$artifactVersion" "${classifier:+$classifier}" "${extension}" 2>/dev/null; then
    echo "Artifact already exists: ${group}:${artifact}:$artifactVersion:${os}"
    exit 0
  fi

  generate_pom=true
  if artifact_exists "$group" "$artifact" "$artifactVersion" "" "pom" 2>/dev/null; then
    generate_pom=false
  fi
fi

currentDir=$(pwd)
cd "$tmpdir"

if ${path}/download-$browser.sh $os $version; then
  majorVersion=$(echo "${version}" | cut -d'.' -f1,2)
  mv "${tmpdir}/${browser}-${version}-${os}.zip" "${tmpdir}/${browser}-${os}.zip"
  jarFile "/${tmpdir}/${browser}-${majorVersion}-${os}" "${browser}-${os}.zip"
  cd "$currentDir"

  file="${tmpdir}/${browser}-${majorVersion}-${os}.jar"

  if [ "$deploy" == "true" ]; then

    mvn -N -e deploy:deploy-file -Durl="${MAVEN_REPO_URL}" \
      -DrepositoryId="${MAVEN_REPO_ID}" \
      -Dfile="$file" \
      -DgroupId="$group" \
      -DartifactId="$artifact" \
      -Dversion="$artifactVersion" \
      ${classifier:+"-Dclassifier=$classifier"} \
      -Dpackaging="${extension}" \
      -DgeneratePom="$generate_pom"

    echo "Artifact deployed: ${group}:${artifact}:version:${os}"
  else
    mvn -N -e install:install-file \
      -Dfile="$file" \
      -DgroupId="$group" \
      -DartifactId="$artifact" \
      -Dversion="$artifactVersion" \
      ${classifier:+"-Dclassifier=$classifier"} \
      -Dpackaging="${extension}"

    echo "Artifact installed: ${group}:${artifact}:version:${os}"
  fi

  rm -Rf tmpdir
else
  echo "Failed to install ${browser}. See error."
  rm -Rf tmpdir
  exit 1
fi
