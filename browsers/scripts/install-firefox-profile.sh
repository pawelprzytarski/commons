#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/commons.sh"

driverVersion=$1
firefoxVersion=$2
os=$3
deploy=$4
currentDir=$(pwd)

group="eu.rarogsoftware.browsers"
artifact="firefox-profile"
artifactVersion=${firefoxVersion}
classifier=${os}
extension="jar"

if [ "$deploy" == "true" ]; then
  if artifact_exists "$group" "$artifact" "$artifactVersion" "${classifier:+$classifier}" "${extension}" 2>/dev/null; then
    echo "Artifact already exists: ${group}:${artifact}:$artifactVersion:${os}"
    exit 0
  fi

  generate_pom=true
  if artifact_exists "$group" "$artifact" "$artifactVersion" "" "pom" 2>/dev/null; then
    generate_pom=false
  fi
fi


case "$os" in
linux64)
  osString="linux64"
  extString="tar.gz"
  ;;
windows)
  osString="win64"
  extString="zip"
  ;;
*)
  fail "Invalid operating system ${os}"
  ;;
esac

tmpdir=$(mktemp -d)

profileDir="firefox-profile-${os}"
profilePath="${tmpdir}/${profileDir}"
mkdir -p ${profilePath}

echo "firefox profile version: ${firefoxVersion} for ${os}" >${profilePath}/profile.package
echo "geckodriver version: ${driverVersion}" >>${profilePath}/profile.package
cp "$(dirname "$0")/firefox/profile.preferences" ${profilePath}/profile.preferences

cd "$tmpdir"

driverArchiveTmp="geckodriver.${extString}"
url="https://github.com/mozilla/geckodriver/releases/download/${driverVersion}/geckodriver-${driverVersion}-${osString}.${extString}"

getFile "${driverArchiveTmp}" "$url"
if [ $? != 0 ]; then
  echo "Unable to download Gecko driver from $url"
  exit 1
fi

case "$os" in
linux64)
  tar -zxf "${driverArchiveTmp}" -C "${profilePath}"
  ;;
windows)
  unzip -d "${profilePath}" "${driverArchiveTmp}" || fail "Unable to unzip geckodriver.zip"
  ;;
esac

cd "${profilePath}"
zipFile "${profilePath}" "*"
profileJarPath="${tmpdir}/firefox-${firefoxVersion}-profile.jar"
cd "${tmpdir}"
jarFile "${tmpdir}/firefox-${firefoxVersion}-profile" "${profileDir}.zip"

file="${profileJarPath}"

if [ "$deploy" == "true" ]; then

  mvn -N -e deploy:deploy-file -Durl="${MAVEN_REPO_URL}" \
    -DrepositoryId="${MAVEN_REPO_ID}" \
    -Dfile="$file" \
    -DgroupId="$group" \
    -DartifactId="$artifact" \
    -Dversion="$artifactVersion" \
    ${classifier:+"-Dclassifier=$classifier"} \
    -Dpackaging="${extension}" \
    -DgeneratePom="$generate_pom"

  echo "Artifact deployed: ${group}:${artifact}:$artifactVersion:${os}"
else
  mvn -N -e install:install-file \
    -Dfile="$file" \
    -DgroupId="$group" \
    -DartifactId="$artifact" \
    -Dversion="$artifactVersion" \
    ${classifier:+"-Dclassifier=$classifier"} \
    -Dpackaging="${extension}"

  echo "Artifact installed: ${group}:${artifact}:$artifactVersion:${os}"
fi

cd "${currentDir}"
rm -Rf "${tmpdir}"
