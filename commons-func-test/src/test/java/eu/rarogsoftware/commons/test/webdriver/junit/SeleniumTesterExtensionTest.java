package eu.rarogsoftware.commons.test.webdriver.junit;

import eu.rarogsoftware.commons.test.webdriver.selenium.WebdriverTester;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({EnvironmentDataExtension.class, SeleniumTesterExtension.class})
class SeleniumTesterExtensionTest {
    @Inject
    WebdriverTester tester;

    @Test
    void testExtensionWorks() {
        assertNotNull(tester.getWebDriver());
    }
}