package eu.rarogsoftware.commons.test.webdriver.junit;

import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith({EnvironmentDataExtension.class, PlaywrightTesterExtension.class})
class PlaywrightTesterExtensionTest {
    @Inject
    WebdriverTester tester;

    @Test
    void testExtensionWorks() throws URISyntaxException, MalformedURLException, InterruptedException {
        assertNotNull(tester);
        tester.navigate(new URI("https://google.com"));
    }
}