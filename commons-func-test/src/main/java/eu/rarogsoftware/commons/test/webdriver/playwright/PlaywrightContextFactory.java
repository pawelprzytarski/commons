package eu.rarogsoftware.commons.test.webdriver.playwright;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType.LaunchOptions;
import com.microsoft.playwright.Playwright;
import eu.rarogsoftware.commons.test.webdriver.XvfbRunner;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PlaywrightContextFactory {
    private static final XvfbRunner xvfbRunner = new XvfbRunner(new File("target/webdriver/xvfb"));
    private BrowserType DEFAULT_BROWSER_TYPE = BrowserType.FIREFOX;

    public PlaywrightContext createDefaultContext(Object testObject) {
        var playwright = createPlaywright();
        var settings = testObject.getClass().getAnnotation(PlaywrightSettings.class);
        var browsers = createDefaultBrowser(playwright, settings != null ? settings.browser() : DEFAULT_BROWSER_TYPE);
        return new PlaywrightContext(playwright, browsers, getDefaultBrowser(browsers));
    }

    private Playwright createPlaywright() {
        if (isXvfbRequired()) {
            return Playwright.create(new Playwright.CreateOptions().setEnv(Map.of("DISPLAY", xvfbRunner.getDisplay())));
        } else {
            return Playwright.create();
        }
    }

    private boolean isXvfbRequired() {
        return Boolean.parseBoolean(System.getProperty("xvfb.enable", "false"))
                && isHeadless();
    }

    private static boolean isHeadless() {
        return Boolean.parseBoolean(System.getProperty("playwright.headless", "true"));
    }

    private Browser getDefaultBrowser(Map<String, Browser> browsers) {
        return browsers.values().stream().findFirst().orElseThrow(() -> new IllegalStateException("Playwright context created without any browser"));
    }

    private Map<String, Browser> createDefaultBrowser(Playwright playwright, BrowserType type) {
        return new HashMap<>(Collections.singletonMap(type.getName(), createBrowser(type, playwright)));
    }

    private Browser createBrowser(BrowserType browserType, Playwright playwright) {
        final LaunchOptions launchOptions = new LaunchOptions().setHeadless(isHeadless());
        return switch (browserType) {
            case CHROME -> playwright.chromium().launch(launchOptions);
            case WEBKIT -> playwright.webkit().launch(launchOptions);
            case FIREFOX -> playwright.firefox().launch(launchOptions);
        };
    }

    public PlaywrightContext updateContextForTest(Method method, PlaywrightContext context) {
        var settings = method.getAnnotation(PlaywrightSettings.class);
        var type = (settings != null ? settings.browser() : DEFAULT_BROWSER_TYPE);
        if (!context.browsers().containsKey(type.getName())) {
            var newBrowser = createBrowser(type, context.playwright());
            context.browsers().put(type.getName(), newBrowser);
        }
        context.changeTestBrowser(type.getName());
        return context;
    }
}
