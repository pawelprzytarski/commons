package eu.rarogsoftware.commons.test.webdriver;

import java.net.MalformedURLException;
import java.net.URI;

/**
 * Interface of all helpers that allow to write web browser based tests using page objects pattern.
 * It contains common functionalities that are supported by all frameworks for web browser tests.
 */
public interface WebdriverTester {
    /**
     * Sets default timeout of operations
     * @param timeout timeout in milliseconds
     */
    void setTimeout(Integer timeout);

    /**
     * Returns default timeout of operations
     * @return  timeout in milliseconds
     */
    int getTimeout();

    /**
     * Navigate main page of tests to specified url. It automatically adds base url to relative addresses.
     * @param url url to navigate to
     */
    void goTo(String url);

    /**
     * Navigate main page of tests to specified url.
     * @param uri uri to navigate to
     */
    void navigate(URI uri) throws MalformedURLException;

    /**
     * Creates new instance of page object and navigates to its target address and binds page object to loaded page in browser
     * (returned by {@link PageObject#getTargetPageUrl()}.
     * @param pageObjectClass type of page object
     * @return newly create and bound page object
     */
    <U extends PageObject> U goTo(Class<U> pageObjectClass);

    /**
     * Navigates to page object target address and binds page object to loaded page in browser
     * (returned by {@link PageObject#getTargetPageUrl()}.
     * @param pageObject page object to navigate to
     * @return bound page object
     */
    <U extends PageObject> U goTo(U pageObject);

    /**
     * Creates and binds page object to currently loaded page in browser without navigating.
     * @param pageObjectClass type of page object
     * @return bound page object
     */
    <U extends PageObject> U bindPage(Class<U> pageObjectClass);

    /**
     * Binds page object to currently loaded page in browser without navigating.
     * @param pageObject page object to navigate to
     * @return bound page object
     */
    <U extends PageObject> U bindPage(U pageObject);

    /**
     * Returns current URL of main page in web browser without base url.
     * @return url of current page
     */
    String getCurrentUrl();

    /**
     * Returns html content of main page in web browser.
     * @return html of page
     */
    String getPageSource();

    /**
     * Returns title of main page in web browser.
     * @return title of page
     */
    String getTitle();

    /**
     * Returns absolute current URL of main page in web browser.
     * @return absolute url of current page
     */
    String getCurrentAbsoluteUrl();

    void setEnvironmentData(EnvironmentData environmentData);
    EnvironmentData getEnvironmentData();
}
