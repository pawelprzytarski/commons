package eu.rarogsoftware.commons.test.utils;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.RelationalPathBase;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.FileSystemResourceAccessor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Tool for quick backup and restore database state for testing purpose.
 * It uses Liquibase changelog to keep state of database and uses QueryDsl to read new data from database
 * It isn't suitable for usage in production environment
 */
@SuppressWarnings("unused")
public class RestoreTool {
    private final String MAIN_CHANGELOG_FILE = "db/migrations/changelog.xml";
    private final String TEST_CHANGELOG_FILE = "db/migrations/testChangelog.xml";
    private String baseDirectory;
    private String migrationsDirectory;
    private String testMigrationsDirectory;
    private final Supplier<Connection> connectionSupplier;

    public RestoreTool(DataSource dataSource) {
        this.connectionSupplier = () -> {
            try {
                return dataSource.getConnection();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        };
    }

    public RestoreTool(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    public void setBaseDirectory(String baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public void setMigrationsDirectory(String migrationsDirectory) {
        this.migrationsDirectory = migrationsDirectory;
    }

    public void setTestMigrationsDirectory(String testMigrationsDirectory) {
        this.testMigrationsDirectory = testMigrationsDirectory;
    }

    /**
     * Copies current Liquibase changelogs and adds changelog with new data from selected tables.
     *
     * @param filename       name of backup file
     * @param tablesToBackup list of tables to back up
     * @throws RestoreToolException when failed to read database state and store it in zip file
     */
    public void backupDatabase(String filename, Collection<RelationalPathBase> tablesToBackup) throws RestoreToolException {
        backupDatabase(filename, tablesToBackup, false);
    }

    /**
     * Copies current Liquibase changelogs and adds changelog with new data from selected tables.
     *
     * @param filename       name of backup file
     * @param tablesToBackup list of tables to back up
     * @param overrideFile   set to true to override existing backup file
     * @throws RestoreToolException when failed to read database state and store it in zip file
     */
    public void backupDatabase(String filename, Collection<RelationalPathBase> tablesToBackup, boolean overrideFile) throws RestoreToolException {
        try {
            Path temporaryDirectory = Files.createTempDirectory("backup").toAbsolutePath();
            FileUtils.copyDirectoryToDirectory(new File(migrationsDirectory + "db"), temporaryDirectory.toFile());
            Path generatedChangelog = Path.of(temporaryDirectory.toString(), "db/migrations/dataChangelog.xml");

            writeChangelogToFile(tablesToBackup, generatedChangelog, temporaryDirectory);
            includeDataChangelogToMainChangelog(temporaryDirectory);
            compressZipfile(Path.of(temporaryDirectory.toString(), "db"), Path.of(temporaryDirectory.toString(), "migration.zip"));
            copyZipToFinalDestination(filename, overrideFile, temporaryDirectory);

            temporaryDirectory.toFile().deleteOnExit();
        } catch (Exception e) {
            throw new RestoreToolException(e);
        }
    }

    private void copyZipToFinalDestination(String filename, boolean overrideFile, Path temporaryDirectory) throws IOException {
        Path target = Path.of(baseDirectory, filename);
        if (overrideFile) {
            target.toFile().delete();
        }
        Files.copy(Path.of(temporaryDirectory.toString(), "migration.zip"), target);
    }

    private void includeDataChangelogToMainChangelog(Path temporaryDirectory) throws IOException {
        Path changelogPath = Path.of(temporaryDirectory.toString(), MAIN_CHANGELOG_FILE);
        String changelogContent = Files.readString(changelogPath);
        changelogContent = changelogContent.replace("</databaseChangeLog>", "<include file=\"db/migrations/dataChangelog.xml\"/></databaseChangeLog>");
        Files.writeString(changelogPath, changelogContent);
    }

    public static void compressZipfile(Path sourceDir, Path outputFile) throws IOException {
        try (ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(outputFile.toFile()))) {
            compressDirectoryToZipfile(sourceDir.getParent(), sourceDir, zipFile);
        }
    }

    private static void compressDirectoryToZipfile(Path rootDir, Path sourceDir, ZipOutputStream out) throws IOException {
        for (File file : sourceDir.toFile().listFiles()) {
            if (file.isDirectory()) {
                compressDirectoryToZipfile(rootDir, Path.of(sourceDir.toString(), file.getName()), out);
            } else {
                ZipEntry entry = new ZipEntry(Path.of(rootDir.relativize(sourceDir).toString(), file.getName()).toString());
                out.putNextEntry(entry);

                try (FileInputStream in = new FileInputStream(Path.of(sourceDir.toString(), file.getName()).toString())) {
                    IOUtils.copy(in, out);
                }
            }
        }
    }

    private void writeChangelogToFile(Collection<RelationalPathBase> tablesToBackup, Path generatedChangelog, Path temporaryDirectory) throws Exception {
        try (Connection currentDatabase = getConnection();
             Connection emptyDatabase = createEmptyH2()) {
            Database mockDb = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(emptyDatabase));
            Liquibase liquibase = new Liquibase(MAIN_CHANGELOG_FILE, new FileSystemResourceAccessor(temporaryDirectory.toFile()), mockDb);
            liquibase.dropAll();
            liquibase.update("dev");
            liquibase.close();

            SQLTemplates templates = new H2Templates();
            SQLQueryFactory currentDbSql = new SQLQueryFactory(templates, () -> currentDatabase);
            SQLQueryFactory emptyDbSql = new SQLQueryFactory(templates, () -> emptyDatabase);

            StringBuilder stringBuilder = new StringBuilder();

            for (RelationalPathBase table : tablesToBackup) {
                List<com.querydsl.core.types.Path<?>> columnsToSave = table.getColumns();

                if (!columnsToSave.isEmpty()) {
                    List<Tuple> newTuples = buildListOfRowsToSave(currentDbSql, emptyDbSql, table, columnsToSave);

                    if (!newTuples.isEmpty()) {
                        saveRowsToXml(stringBuilder, table, columnsToSave, newTuples);
                    }
                }
            }

            if (!stringBuilder.isEmpty()) {
                writeDataChangelogToFile(generatedChangelog, stringBuilder);
            }
        }
    }

    private void writeDataChangelogToFile(Path generatedChangelog, StringBuilder stringBuilder) throws IOException {
        stringBuilder.append("</changeSet></databaseChangeLog>");
        try (OutputStream outputStream = new FileOutputStream(generatedChangelog.toFile())) {
            outputStream.write("""
                    <databaseChangeLog
                            xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                            xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext
                           http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd
                           http://www.liquibase.org/xml/ns/dbchangelog
                           http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.1.xsd">
                        <changeSet author="autogenerated" id="backup_data">
                    """.getBytes(StandardCharsets.UTF_8));
            outputStream.write(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
        }
    }

    private void saveRowsToXml(StringBuilder stringBuilder, RelationalPathBase table, List<com.querydsl.core.types.Path<?>> columnsToSave, List<Tuple> newTuples) {
        for (Tuple row : newTuples) {
            stringBuilder.append("<insert  tableName=\"");
            stringBuilder.append(table.getTableName());
            stringBuilder.append("\">");
            for (com.querydsl.core.types.Path<?> column : columnsToSave) {
                Object value = row.get(column);
                if (value != null) {
                    stringBuilder.append("<column  name=\"");
                    stringBuilder.append(column.getMetadata().getName());
                    stringBuilder.append("\"  value=\"");
                    stringBuilder.append(value.toString());
                    stringBuilder.append("\"/>");
                }
            }
            stringBuilder.append("</insert>");
        }
    }

    private List<Tuple> buildListOfRowsToSave(SQLQueryFactory currentDbSql, SQLQueryFactory emptyDbSql, RelationalPathBase table, List<com.querydsl.core.types.Path<?>> columnsToSave) {
        List<Tuple> originalTuples = emptyDbSql.select(columnsToSave.toArray(new Expression[0]))
                .from(table)
                .fetch();
        List<Tuple> currentTuples = currentDbSql.select(columnsToSave.toArray(new Expression[0]))
                .from(table)
                .fetch();

        return currentTuples.stream()
                .distinct()
                .filter(tuple -> !originalTuples.contains(tuple))
                .collect(Collectors.toList());
    }

    private Connection createEmptyH2() throws SQLException {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:mem:db");
        ds.setUser("sa");
        ds.setPassword("sa");
        return ds.getConnection();
    }

    /**
     * Drops current database and restore state from backup then apply newer migrations
     *
     * @param filename path to zip file with backup
     * @throws RestoreToolException when liquibase fails to restore database or when is problem with reading migrations
     */
    public void restoreDatabase(String filename) throws RestoreToolException {
        try {
            Path temporaryDirectory = Files.createTempDirectory("backup").toAbsolutePath();
            unpackBackupZipToTempDir(filename, temporaryDirectory);
            restoreDatabaseWithLiquibase(temporaryDirectory);
            restoreDatabaseWithLiquibase(Path.of(migrationsDirectory));
        } catch (Exception e) {
            throw new RestoreToolException(e);
        }
    }

    private void restoreDatabaseWithLiquibase(Path changelogDirectory) throws Exception {
        try (Connection connection = getConnection()) {
            Liquibase liquibase = new Liquibase(MAIN_CHANGELOG_FILE, new FileSystemResourceAccessor(new File(changelogDirectory.toString())), new JdbcConnection(connection));
            liquibase.dropAll();
            liquibase.update("dev");
            liquibase.close();
        }
    }

    private Connection getConnection() {
        return connectionSupplier.get();
    }

    private void unpackBackupZipToTempDir(String filename, Path temporaryDirectory) throws IOException {
        Path sourceFile = Path.of(baseDirectory, filename);
        try (ZipInputStream zipFile = new ZipInputStream(new FileInputStream(sourceFile.toFile()))) {
            ZipEntry zipEntry;
            while ((zipEntry = zipFile.getNextEntry()) != null) {
                Path unzipPath = Path.of(temporaryDirectory.toString(), zipEntry.getName());
                FileUtils.forceMkdirParent(unzipPath.toFile());
                try (FileOutputStream out = new FileOutputStream(unzipPath.toFile())) {
                    IOUtils.copy(zipFile, out);
                }
            }
        }
    }

    /**
     * Drops current database and restore clean database
     *
     * @throws RestoreToolException when liquibase fails to restore database or when is problem with reading migrations
     */
    public void restoreCleanDatabase() throws RestoreToolException {
        try {
            Path temporaryDirectory = Files.createTempDirectory("backup").toAbsolutePath();

            FileUtils.copyDirectoryToDirectory(new File(migrationsDirectory + "db"), temporaryDirectory.toFile());
            FileUtils.copyDirectoryToDirectory(new File(testMigrationsDirectory + "db"), temporaryDirectory.toFile());

            try (Connection connection = getConnection()) {
                Liquibase liquibase = new Liquibase(TEST_CHANGELOG_FILE, new FileSystemResourceAccessor(new File(temporaryDirectory.toString())), new JdbcConnection(connection));
                liquibase.dropAll();
                liquibase.update("dev");
                liquibase.close();
            }
        } catch (Exception e) {
            throw new RestoreToolException(e);
        }
    }
}
