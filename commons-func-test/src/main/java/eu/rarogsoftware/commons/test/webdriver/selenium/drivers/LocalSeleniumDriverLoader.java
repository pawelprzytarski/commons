package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public abstract class LocalSeleniumDriverLoader implements SeleniumDriverLoader{
    protected static final String DRIVERS_LOCATION = "target/selenium/drivers/";
    private final Logger logger = LoggerFactory.getLogger(LocalSeleniumDriverLoader.class);
    private final String SELENIUM_CACHE_DIR = ".selenium/";
    private final String DRIVERS_CACHE_DIRECTORY = SELENIUM_CACHE_DIR + "drivers/";
    private final String BROWSERS_CACHE_DIR = SELENIUM_CACHE_DIR + "browsers/";

    public void validatePrerequisites() {
        validateRuby();
        validateRubySeleniumDriver();
    }

    private void validateRuby() {
        try {
            var process = Runtime.getRuntime().exec("ruby -v");
            process.waitFor();
            String rubyVersion = new String(process.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
            if (rubyVersion.length() < 4 || !rubyVersion.startsWith("ruby")) {
                logger.error("Ruby not installed. Ruby is required to use selenium driver. Please install ruby to continue");
                throw new IllegalStateException("Ruby not installed");
            }
        } catch (IOException | InterruptedException e) {
            logger.error("Cannot verify if ruby is installed.", e);
            throw new IllegalStateException("Exception during validating ruby installation", e);
        }
    }

    private void validateRubySeleniumDriver() {
        try {
            String gemCommand = "gem list";
            if (SystemUtils.IS_OS_WINDOWS) {
                gemCommand = "gem.cmd list";
            }
            var process = Runtime.getRuntime().exec(gemCommand);
            process.waitFor();
            String installedGems = new String(process.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
            if (installedGems.length() <= 0 || !installedGems.contains("selenium-webdriver")) {
                logger.error("selenium-webdriver not installed in ruby. Please install it with `gem install selenium-webdriver`");
                throw new IllegalStateException("selenium-webdriver not installed");
            }
        } catch (IOException | InterruptedException e) {
            logger.error("Cannot verify if selenium-webdriver is installed.", e);
            throw new IllegalStateException("Exception during validating selenium-webdriver installation", e);
        }
    }

    protected void downloadDriver(String driverFilename) {
        var driversDirectory = new File(DRIVERS_LOCATION);
        if (!driversDirectory.exists()) {
            driversDirectory.mkdirs();
        }
        var driverFile = new File(DRIVERS_LOCATION + driverFilename);

        if (driverFile.exists()) {
            logger.info("Driver already downloaded");
            return;
        }

        File cachedDriverFile = Path.of(getDirectoryOfCachedDriver().toString(), driverFilename).toAbsolutePath().toFile();

        if (cachedDriverFile.exists()) {
            logger.info("Using cached driver");
            copyFile(cachedDriverFile, driverFile);
        } else {
            downloadAndUnpackDriver(driverFile, cachedDriverFile);
        }
    }

    private void downloadAndUnpackDriver(File driverFile, File cachedDriverFile) {
        String url = constructDownloadAddress();
        logger.info("Downloading firefox driver from {}", url);
        try {
            logger.info("Start downloading driver");
            File temporaryFile = downloadToTemporaryFile(url);
            logger.info("Driver successfully downloaded");

            getDirectoryOfCachedDriver().toFile().mkdirs();
            unpackDriver(temporaryFile);
            copyFile(cachedDriverFile, driverFile);
        } catch (IOException | InterruptedException e) {
            logger.error("Exception during downloading and unpacking driver", e);
            throw new RuntimeException("Cannot download driver", e);
        }
    }

    protected File downloadToTemporaryFile(String url) throws IOException {
        File temporaryFile = File.createTempFile("download", "temp");
        temporaryFile.deleteOnExit();

        FileUtils.copyURLToFile(
                new URL(url),
                temporaryFile,
                10000,
                10000);
        return temporaryFile;
    }

    protected abstract void unpackDriver(File temporaryFile) throws IOException, InterruptedException;

    protected void copyFile(File source, File destination) {
        try {
            FileUtils.copyFile(source, destination);
        } catch (IOException e) {
            logger.error("Coping cached version of driver failed", e);
            throw new RuntimeException("Cannot copy cached version of driver", e);
        }
    }

    protected Path getDirectoryOfCachedDriver() {
        return Path.of(SystemUtils.USER_HOME, DRIVERS_CACHE_DIRECTORY, getDriverType(), getDriverVersion());
    }

    protected boolean getBooleanProperty(String name) {
        String property = System.getProperty(name);
        if(property == null) {
            property = System.getenv(name);
        }
        return Boolean.parseBoolean(property);
    }

    protected Path getDirectoryOfCachedBrowsers() {
        return Path.of(SystemUtils.USER_HOME, BROWSERS_CACHE_DIR);
    }

    protected abstract String constructDownloadAddress();

    protected abstract String getDriverType();

    protected abstract String getDriverVersion();

    public abstract void downloadDriver();

    public abstract WebDriver initializeDriver();
}
