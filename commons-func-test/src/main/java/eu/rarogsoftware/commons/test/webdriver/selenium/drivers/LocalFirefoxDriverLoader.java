package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class LocalFirefoxDriverLoader extends LocalSeleniumDriverLoader {
    private static final Logger logger = LoggerFactory.getLogger(LocalFirefoxDriverLoader.class);
    private static final String DOWNLOAD_URL = "https://github.com/mozilla/geckodriver/releases/download/v%1$s/geckodriver-v%1$s-%2$s";
    private static final String DEFAULT_DRIVER_VERSION = "0.30.0";
    private static final String DRIVER_FILENAME = "geckodriver";
    private static final String FIREFOX_DRIVER_VERSION_PROPERTY = "selenium.firefox.driver.version";

    @Override
    public void downloadDriver() {
        downloadDriver(DRIVER_FILENAME);
    }

    @Override
    protected void unpackDriver(File temporaryFile) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        String cacheDirectory = getDirectoryOfCachedDriver().toAbsolutePath().toString();
        String temporaryFileAbsolutePath = temporaryFile.getAbsolutePath();
        if (SystemUtils.IS_OS_WINDOWS) {
            runtime.exec(String.format("tar -xf %1$s -C %2$s\\", temporaryFileAbsolutePath, cacheDirectory)).waitFor();
            runtime.exec(String.format("cmd /C \"move %2$s\\%1$s.exe %2$s\\%1$s\"", DRIVER_FILENAME, cacheDirectory)).waitFor();
        } else {
            runtime.exec(String.format("tar -xzf %s -C %s/", temporaryFileAbsolutePath, cacheDirectory)).waitFor();
        }
    }

    @Override
    protected String constructDownloadAddress() {
        String driverType = "linux64.tar.gz";
        if (SystemUtils.IS_OS_WINDOWS) {
            driverType = "win64.zip";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            driverType = "macos.tar.gz";
        }

        String driverVersion = getDriverVersion();

        return String.format(DOWNLOAD_URL, driverVersion, driverType);
    }

    @Override
    protected String getDriverType() {
        return "firefox";
    }

    @Override
    protected String getDriverVersion() {
        return System.getProperty(FIREFOX_DRIVER_VERSION_PROPERTY, DEFAULT_DRIVER_VERSION);
    }

    @Override
    public WebDriver initializeDriver() {
        System.setProperty("webdriver.gecko.driver", Path.of(DRIVERS_LOCATION, DRIVER_FILENAME).toAbsolutePath().toString());
        if(!Path.of(DRIVERS_LOCATION, DRIVER_FILENAME).toFile().setExecutable(true, false)){
            logger.warn("Cannot make geckodriver executable");
        }
        FirefoxOptions options = new FirefoxOptions();
        if (!getBooleanProperty("selenium.disable.headless")) {
            options.setHeadless(true);
        }
        if (!getBooleanProperty("selenium.info.logs.enabled")) {
            options.setLogLevel(FirefoxDriverLogLevel.ERROR);
        }
        if (getBooleanProperty("selenium.debug.logs.enabled")) {
            options.setLogLevel(FirefoxDriverLogLevel.DEBUG);
        }
        return new FirefoxDriver(options);
    }
}
