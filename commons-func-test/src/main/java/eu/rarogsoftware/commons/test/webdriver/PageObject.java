package eu.rarogsoftware.commons.test.webdriver;

/**
 * Base for all Page objects that will be supported by {@link WebdriverTester}.
 */
public abstract class PageObject {
    private WebdriverTester tester;

    public void setTester(WebdriverTester tester) {
        this.tester = tester;
    }

    /**
     * Get default timeout for all operations.
     * @return operations timeout
     */
    public int getTimeout(){
        return tester.getTimeout();
    }

    /**
     * Returns instance of tester that created/bound this page object
     * @return parent tester
     */
    protected WebdriverTester getTester() {
        return tester;
    }

    /**
     * Returns target url that should be loaded as part of {@link WebdriverTester#goTo(PageObject)}
     * @return Page target url
     */
    public abstract String getTargetPageUrl();

    /**
     * Checks if current page is supported by current page object
     * @return
     */
    public abstract boolean isAt();

    /**
     * Returns real url of current page without base url
     * @return url without base url
     */
    public abstract String getCurrentUrl();

    /**
     * Returns html content of current page
     * @return html of current page
     */
    public abstract String getPageSource();

    /**
     * Returns real title of current page
     * @return title of page
     */
    public abstract String getTitle();

    /**
     * Returns real absolute url of current page
     * @return absolute url
     */
    public abstract String getCurrentAbsoluteUrl();
}
