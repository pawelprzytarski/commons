package eu.rarogsoftware.commons.test.webdriver.junit;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightContext;
import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightContextFactory;
import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightSettings;
import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

/**
 * Starts Playwright framework for testing with web browser. Extension automatically manages and clean Playwright framework
 * so no interaction from developer is required.
 * <p>
 * Extension exposes Playwright objects like {@link BrowserContext} or {@link Page} as well as helpful tool {@link WebdriverTester}.
 * All object are injected automatically to fields annotated with {@link javax.inject.Inject}. Injected objects are automatically
 * managed by extension so avoid manual cleaning them.
 * <p>
 * To use extension developer must import it using jUnit5 annotation {@link ExtendWith}:
 * <pre>
 * 	&#64;ExtendWith({PlaywrightTesterExtension.class})
 * 	class JUnit5TestClass {
 * 	    &#64;Inject
 * 	    WebdriverTester tester;
 * 	    &#64;Inject
 * 	    Browser browser;
 * 	    &#64;Inject
 * 	    BrowserContext context;
 * 	    &#64;Inject
 * 	    Page page;
 *
 * 	    &#64;Test
 * 	    void junit5TestMethod() {
 * 	        // ...
 *        }
 *    }
 * </pre>
 * <p>
 * It is possible to control behaviour of Playwright with {@link PlaywrightSettings} annotation to change
 * browser that runs tests or clear playwright context before test.
 * <pre>
 *  /* all tests in class will start with chromium unless annotation on test level changes it,
 *     and each test will start with clear context
 *  *\/
 *  &#64;PlaywrightSettings(browser = BrowserType.CHROME, contextCreation=PlaywrightSettings.BrowserContextScope.METHOD)
 * 	&#64;ExtendWith({PlaywrightTesterExtension.class})
 * 	class JUnit5TestClass {
 * 	    &#64;Inject
 * 	    WebdriverTester tester;
 * 	    &#64;Inject
 * 	    Browser browser;
 * 	    &#64;Inject
 * 	    BrowserContext context;
 * 	    &#64;Inject
 * 	    Page page;
 *
 * 	    &#64;Test
 * 	    void junit5TestMethod() {
 * 	        // test will run with class level settings
 *        }
 *
 * 	    &#64;Test
 * 	    &#64;PlaywrightSettings(browser = BrowserType.FIREFOX)
 * 	    void firefoxTest() {
 * 	        // test will run with firefox
 *        }
 *    }
 * </pre>
 *
 * @see WebdriverTester
 * @see PlaywrightSettings
 * @see Playwright
 * @since 0.1.0
 */
public class PlaywrightTesterExtension implements TestInstancePostProcessor, AfterAllCallback, BeforeEachCallback, AfterEachCallback, AfterTestExecutionCallback {
    public static final String CONTEXT_KEY = PlaywrightTesterExtension.class.getName() + "_playwrightContext";
    public static final String BROWSER_CONTEXT_KEY = PlaywrightTesterExtension.class.getName() + "_browserContext";
    public static final String CLOSEABLE_BROWSER_CONTEXT_KEY = PlaywrightTesterExtension.class.getName() + "_browserContextToCleanup";
    private static final Pattern PATTERN = Pattern.compile("[^A-Za-z0-9_\\-]");
    private final Logger logger = LoggerFactory.getLogger(PlaywrightTesterExtension.class);
    private final PlaywrightContextFactory playwrightContextFactory = new PlaywrightContextFactory();

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        if (store.get(CONTEXT_KEY) != null) {
            return;
        }
        var defaultContext = playwrightContextFactory.createDefaultContext(testInstance);
        store.put(CONTEXT_KEY, defaultContext);
        ExtensionHelpers.setFieldValueIfExists(testInstance, Playwright.class, defaultContext.playwright());
        ExtensionHelpers.setFieldValueIfExists(testInstance, Browser.class, defaultContext.testBrowser());
        var settings = context.getRequiredTestClass().getAnnotation(PlaywrightSettings.class);
        if (settings == null || settings.contextCreation() == PlaywrightSettings.BrowserContextScope.TEST_INSTANCE) {
            store.put(BROWSER_CONTEXT_KEY, createBrowserContextInTestInstance(testInstance, defaultContext));
        }
    }

    private static InstanceBrowserContext createBrowserContextInTestInstance(Object testInstance, PlaywrightContext defaultContext) throws IllegalAccessException {
        var browserContext = defaultContext.testBrowser().newContext();
        var page = browserContext.newPage();
        var tester = new WebdriverTester(browserContext, page);
        ExtensionHelpers.setFieldValueIfExists(testInstance, BrowserContext.class, browserContext);
        ExtensionHelpers.setFieldValueIfExists(testInstance, Page.class, page);
        ExtensionHelpers.setFieldValueIfExists(testInstance, WebdriverTester.class, tester);
        return new InstanceBrowserContext(browserContext, page, tester);
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        ((PlaywrightContext) store.get(CONTEXT_KEY)).close();
        store.remove(CONTEXT_KEY);
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        var testInstance = context.getRequiredTestInstance();
        var defaultContext = ((PlaywrightContext) store.get(CONTEXT_KEY));
        var methodSettings = context.getRequiredTestMethod().getAnnotation(PlaywrightSettings.class);
        var browserContextInstance = (InstanceBrowserContext) store.get(BROWSER_CONTEXT_KEY);
        playwrightContextFactory.updateContextForTest(context.getRequiredTestMethod(), defaultContext);
        ExtensionHelpers.setFieldValueIfExists(testInstance, Browser.class, defaultContext.testBrowser());
        if (browserContextInstance == null
                || defaultContext.testBrowser() != defaultContext.defaultBrowser()
                || (methodSettings != null && methodSettings.contextCreation() == PlaywrightSettings.BrowserContextScope.METHOD)) {
            store.put(CLOSEABLE_BROWSER_CONTEXT_KEY, createBrowserContextInTestInstance(testInstance, defaultContext));
        } else {
            ExtensionHelpers.setFieldValueIfExists(testInstance, BrowserContext.class, browserContextInstance.context());
            ExtensionHelpers.setFieldValueIfExists(testInstance, Page.class, browserContextInstance.page());
            ExtensionHelpers.setFieldValueIfExists(testInstance, WebdriverTester.class, browserContextInstance.tester());
        }
        var envData = ExtensionHelpers.getEnvironmentData(context);
        ExtensionHelpers.getFieldValue(testInstance, WebdriverTester.class).setEnvironmentData(envData);
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        var browserContextInstance = (InstanceBrowserContext) store.get(CLOSEABLE_BROWSER_CONTEXT_KEY);
        if (browserContextInstance != null) {
            browserContextInstance.context().close();
        }
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        var browserContextInstance = (InstanceBrowserContext) store.get(CLOSEABLE_BROWSER_CONTEXT_KEY);
        if (browserContextInstance == null) {
            browserContextInstance = (InstanceBrowserContext) store.get(BROWSER_CONTEXT_KEY);
        }
        makeScreenshotIfFailure(context, browserContextInstance.context());
    }

    private void makeScreenshotIfFailure(ExtensionContext extensionContext, BrowserContext browserContext) {
        if (extensionContext.getExecutionException().isPresent()) {
            var screenshotDirectory = System.getProperty("playwright.screenshot.directory", "target/reports/screenshots/");
            var className = sanitizeString(extensionContext.getTestClass().map(Class::getName).orElse(""));
            var testName = sanitizeString(extensionContext.getDisplayName());
            var date = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")
                    .withZone(ZoneId.of("UTC"))
                    .format(Instant.now());
            var sequencer = new AtomicLong(0);
            browserContext.pages()
                    .forEach(page -> {
                        long sequence = sequencer.incrementAndGet();
                        var screenshotPath = Path.of(screenshotDirectory, className, testName + date + "_" + sequence + ".png");
                        var fullPageScreenshotPath = Path.of(screenshotDirectory, className, testName + date + "_" + sequence + "_full.png");
                        var htmlDumpPath = Path.of(screenshotDirectory, className, testName + date + "_" + sequence + ".html");
                        logger.info("Test failed. Current url: `{}` . Saving screenshot as {}. Saving page content as {}",
                                page.url(),
                                screenshotPath.toAbsolutePath(),
                                htmlDumpPath.toAbsolutePath());

                        page.screenshot(new Page.ScreenshotOptions().setFullPage(false).setPath(screenshotPath.toAbsolutePath()));
                        page.screenshot(new Page.ScreenshotOptions().setFullPage(true).setPath(fullPageScreenshotPath.toAbsolutePath()));
                        try {
                            FileUtils.write(htmlDumpPath.toFile(), page.content(), StandardCharsets.UTF_8);
                        } catch (IOException e) {
                            logger.error("Failed to save screenshot to selected location", e);
                        }
                    });
        }
    }

    private String sanitizeString(String string) {
        return PATTERN.matcher(string).replaceAll("_");
    }

    public static InstanceBrowserContext getContext(ExtensionContext context) {
        var store = ExtensionHelpers.getStore(context);
        var browserContextInstance = (InstanceBrowserContext) store.get(CLOSEABLE_BROWSER_CONTEXT_KEY);
        if (browserContextInstance == null) {
            browserContextInstance = (InstanceBrowserContext) store.get(BROWSER_CONTEXT_KEY);
        }
        return browserContextInstance;
    }

    public record InstanceBrowserContext(BrowserContext context, Page page, WebdriverTester tester) {
    }
}
