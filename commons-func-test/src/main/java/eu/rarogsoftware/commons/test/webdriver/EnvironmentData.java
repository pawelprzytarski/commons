package eu.rarogsoftware.commons.test.webdriver;

import java.util.Properties;

/**
 * Contains basing data about started instance like base url
 */
public record EnvironmentData(String protocol, String host, String port, String contextPath,
                              Properties additionalProperties) {
    public static final EnvironmentData DEFAULT = new EnvironmentData("http", "localhost", "8080", "/", new Properties());

    public EnvironmentData(String protocol, String host, String port, String contextPath, Properties additionalProperties) {
        this.port = port;
        this.host = host;
        this.contextPath = normalizeContextPath(contextPath);
        this.protocol = protocol;
        this.additionalProperties = additionalProperties;
    }

    private String normalizeContextPath(String contextPath) {
        if (contextPath.length() == 0) {
            return "";
        }
        return (contextPath.startsWith("/") ? "" : "/")
                + ((contextPath.endsWith("/") ? contextPath.substring(0, contextPath.length() - 1) : contextPath));
    }

    public String baseUrl() {
        return String.format("%s://%s:%s%s/", protocol, host, port, contextPath);
    }
}
