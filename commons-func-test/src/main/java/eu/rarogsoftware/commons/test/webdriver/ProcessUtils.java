package eu.rarogsoftware.commons.test.webdriver;

import org.apache.commons.io.IOUtils;

import java.io.*;

public class ProcessUtils {
    public static void runProcessInForeground(ProcessBuilder procBuilder) {
        Process process;
        try {
            process = procBuilder.start();
            process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Unable to execute " + procBuilder + " as execution was interrupted.");
        } catch (IOException e) {
            throw new RuntimeException("Unable to start process", e);
        }
        if (process.exitValue() != 0) {
            throw new RuntimeException("Unable to execute " + procBuilder + " as returned error code " + process.exitValue());
        }
    }

    public static Process runProcessInBackground(ProcessBuilder procBuilder) {
        Process process;
        try {
            process = procBuilder.start();
        } catch (IOException e) {
            throw new RuntimeException("Unable to start process", e);
        }
        return process;
    }
}