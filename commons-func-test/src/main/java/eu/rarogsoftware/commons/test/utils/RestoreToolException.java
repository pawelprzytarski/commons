package eu.rarogsoftware.commons.test.utils;

public class RestoreToolException extends Exception {
    public RestoreToolException(Throwable cause) {
        super(cause);
    }
}
