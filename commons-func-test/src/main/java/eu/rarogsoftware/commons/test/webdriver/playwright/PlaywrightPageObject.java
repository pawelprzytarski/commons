package eu.rarogsoftware.commons.test.webdriver.playwright;

import com.microsoft.playwright.Page;
import eu.rarogsoftware.commons.test.webdriver.PageObject;

/**
 * Version of page object that supports Playwright
 * @since 0.1.0
 */
public abstract class PlaywrightPageObject extends PageObject {
    private Page page;

    protected Page getPage() {
        return page;
    }

    public void bind(Page page) {
        this.page = page;
        page.setDefaultTimeout(getTimeout());
        page.setDefaultNavigationTimeout(getTimeout());
        bind();
    }

    /**
     * Actual binding method to be implemented by page object. Method must bind all necessary elements and wait
     * for page to load.
     */
    public abstract void bind();

    @Override
    public String getCurrentUrl() {
        var environmentData = getTester().getEnvironmentData();
        if (environmentData == null) {
            return getCurrentAbsoluteUrl();
        } else {
            var currentUrl = getCurrentAbsoluteUrl();
            var baseUrl = environmentData.baseUrl();
            if (currentUrl.startsWith(baseUrl)) {
                return currentUrl.substring(baseUrl.length() - 1);
            } else {
                return currentUrl;
            }
        }
    }

    @Override
    public String getPageSource() {
        return page.content();
    }

    @Override
    public String getTitle() {
        return page.title();
    }

    @Override
    public String getCurrentAbsoluteUrl() {
        return page.url();
    }
}
