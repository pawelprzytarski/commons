package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LocalChromiumDriverLoader extends LocalSeleniumDriverLoader {
    private static final String DOWNLOAD_URL = "https://chromedriver.storage.googleapis.com/%1$s/chromedriver_%2$s";
    private static final String DEFAULT_DRIVER_VERSION = "87.0.4280.88";
    private static final String DRIVER_FILENAME = "chromedriver";
    private static final String CHROMIUM_DRIVER_VERSION_PROPERTY = "selenium.chromium.version";

    @Override
    public void downloadDriver() {
        downloadDriver(DRIVER_FILENAME);
    }

    @Override
    protected void unpackDriver(File temporaryFile) throws IOException {
        Runtime.getRuntime().exec(String.format("tar -xf %s %s/", temporaryFile.getAbsolutePath(), getDirectoryOfCachedDriver().toAbsolutePath().toString()));
        unzipDriver(temporaryFile);
    }

    private void unzipDriver(File temporaryFile) throws IOException {
        File destDir = getDirectoryOfCachedDriver().toFile();
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(temporaryFile));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        String name = zipEntry.getName();
        if (name.endsWith(".exe")) {
            name = name.substring(0, name.length() - 4);
        }
        File destFile = new File(destinationDir, name);

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    @Override
    protected String constructDownloadAddress() {
        String driverType = "linux64.zip";
        if (SystemUtils.IS_OS_WINDOWS) {
            driverType = "win32.zip";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            driverType = "mac64.zip";
        }

        String driverVersion = getDriverVersion();

        return String.format(DOWNLOAD_URL, driverVersion, driverType);
    }

    @Override
    protected String getDriverType() {
        return "chromium";
    }

    @Override
    protected String getDriverVersion() {
        return System.getProperty(CHROMIUM_DRIVER_VERSION_PROPERTY, DEFAULT_DRIVER_VERSION);
    }

    @Override
    public WebDriver initializeDriver() {
        System.setProperty("webdriver.chrome.driver", Path.of(DRIVERS_LOCATION, DRIVER_FILENAME).toAbsolutePath().toString());
        ChromeOptions options = new ChromeOptions();
        if(!getBooleanProperty("selenium.disable.headless")) {
            options.setHeadless(true);
        }
        return new ChromeDriver(options);
    }
}
