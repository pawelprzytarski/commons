package eu.rarogsoftware.commons.test.webdriver.selenium;

import eu.rarogsoftware.commons.test.webdriver.selenium.drivers.SeleniumDriverFactory;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class WebdriverConfig {
    private final SeleniumDriverFactory.SupportedDriverType driverType;
    private WebDriver driver;

    public WebdriverConfig() {
        driverType = null;
        driver = SeleniumDriverFactory.getInstance().getDriver();
        initialize();
    }

    public WebdriverConfig(SeleniumDriverFactory.SupportedDriverType driverType) {
        this.driverType = driverType;
        driver = SeleniumDriverFactory.getInstance().getDriver(driverType);
        initialize();
    }

    private void initialize() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void close() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public void reinitialize() {
        if (driver == null) {
            if(driverType == null) {
                driver = SeleniumDriverFactory.getInstance().getDriver();
            }else {
                driver = SeleniumDriverFactory.getInstance().getDriver(driverType);
            }
        }
    }

    public void clean() {
        if (isBrowseReuseEnabled()) {
            driver.manage().deleteAllCookies();
        } else {
            close();
        }
    }

    private boolean isBrowseReuseEnabled() {
        return Boolean.parseBoolean(System.getProperty("selenium.reuse.browser", "true"));
    }
}
