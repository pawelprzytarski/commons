package eu.rarogsoftware.commons.test.webdriver.selenium;

import eu.rarogsoftware.commons.test.webdriver.AbstractWebdriverTester;
import eu.rarogsoftware.commons.test.webdriver.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.Duration;

public class WebdriverTester extends AbstractWebdriverTester {
    private WebdriverConfig webdriverConfig;

    public WebdriverTester(WebdriverConfig webdriverConfig) {
        this.webdriverConfig = webdriverConfig;
    }

    public WebdriverTester() {
    }

    public WebDriver getWebDriver() {
        return webdriverConfig.getDriver();
    }

    public void navigate(URI uri) throws MalformedURLException {
        getWebDriver().navigate().to(uri.toURL());
    }

    public <U extends PageObject> U bindPage(U pageObject) {
        getPageObjectSuperClass();
        pageObject.setTester(this);
        PageFactory.initElements(new AjaxElementLocatorFactory(getWebDriver(), pageObject.getTimeout()), pageObject);
        new WebDriverWait(getWebDriver(), Duration.ofSeconds(pageObject.getTimeout()))
                .until(webDriver -> ((SeleniumPageObject) pageObject).isAt(webDriver));
        return pageObject;
    }

    public WebdriverConfig getWebdriverConfig() {
        return webdriverConfig;
    }

    public WebElement getElement(By by) {
        return new WebDriverWait(webdriverConfig.getDriver(), Duration.ofSeconds(timeout))
                .until(webDriver -> webDriver.findElement(by));
    }


    public String getPageSource() {
        return getWebDriver().getPageSource();
    }

    public String getTitle() {
        return getWebDriver().getTitle();
    }

    public String getCurrentAbsoluteUrl() {
        return getWebDriver().getCurrentUrl();
    }

    public void setWebdriverConfig(WebdriverConfig webdriverConfig) {
        this.webdriverConfig = webdriverConfig;
    }

    @Override
    protected Class<SeleniumPageObject> getPageObjectSuperClass() {
        return SeleniumPageObject.class;
    }
}
