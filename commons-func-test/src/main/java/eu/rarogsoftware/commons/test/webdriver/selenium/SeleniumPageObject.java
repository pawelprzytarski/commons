package eu.rarogsoftware.commons.test.webdriver.selenium;

import eu.rarogsoftware.commons.test.webdriver.PageObject;
import org.openqa.selenium.WebDriver;

public abstract class SeleniumPageObject extends PageObject {
    protected WebDriver getWebdriver() {
        return ((WebdriverTester) getTester()).getWebDriver();
    }

    @Override
    public boolean isAt() {
        return isAt(getWebdriver());
    }

    public abstract boolean isAt(WebDriver webDriver);

    @Override
    public String getCurrentUrl() {
        return getTester().getCurrentUrl();
    }

    @Override
    public String getPageSource() {
        return getTester().getPageSource();
    }

    @Override
    public String getTitle() {
        return getTester().getTitle();
    }

    @Override
    public String getCurrentAbsoluteUrl() {
        return getTester().getCurrentAbsoluteUrl();
    }
}
