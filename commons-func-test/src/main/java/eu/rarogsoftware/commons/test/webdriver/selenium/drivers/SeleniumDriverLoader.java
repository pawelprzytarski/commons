package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import org.openqa.selenium.WebDriver;

public interface SeleniumDriverLoader {
    void validatePrerequisites();
    void downloadDriver();
    WebDriver initializeDriver();
}
