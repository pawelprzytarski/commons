package eu.rarogsoftware.commons.test.webdriver.junit;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Properties;

/**
 * Reads environmental data from system properties, file "localconfig.properties" in classpath
 * or use default data and injects test object with {@link EnvironmentData} instance.
 */
public class EnvironmentDataExtension implements BeforeEachCallback, BeforeAllCallback {
    private static final String PORT_PROPERTIES_KEY = "test.container.port";
    private static final String HOST_PROPERTIES_KEY = "test.container.host";
    private static final String CONTEXT_PATH_PROPERTIES_KEY = "test.container.context.path";
    private static final String PROTOCOL_PROPERTIES_KEY = "test.container.protocol";
    public static final String STORE_BEFORE_ALL_KEY = EnvironmentData.class.getName() + "_beforeAll_data";
    public static final String STORE_BEFORE_EACH_KEY = EnvironmentData.class.getName() + "_beforeEach_data";
    private EnvironmentData loadedData = null;

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        if (extensionContext.getTestInstance().isPresent()) {
            var o = extensionContext.getTestInstance().get();
            var loadedData = retrieveEnvironmentData(extensionContext);
            var field = getDataField(o);
            if (field != null) {
                field.set(o, loadedData);
            }
        }

    }

    private EnvironmentData retrieveEnvironmentData(ExtensionContext extensionContext) {
        ExtensionContext.Store store = getStore(extensionContext);
        var loadedData = store.get(STORE_BEFORE_EACH_KEY, EnvironmentData.class);
        if (loadedData == null) {
            loadedData = getLoadedData(extensionContext.getRequiredTestClass());
            store.put(STORE_BEFORE_EACH_KEY, loadedData);
        }
        return loadedData;
    }

    private Field getDataField(Object o) {
        return getDataField(o, o.getClass());
    }

    private Field getDataField(Object o, Class<?> clazz) {
        if (clazz.equals(Object.class)) {
            return null;
        }
        Field dataField = getDataField(o, clazz.getSuperclass());
        for (Field field : clazz.getDeclaredFields()) {
            if (EnvironmentData.class.isAssignableFrom(field.getType())
                    && (field.isAnnotationPresent(Inject.class))) {
                if (!field.canAccess(o)) {
                    field.setAccessible(true);
                }
                dataField = field;
            }
        }
        return dataField;
    }

    private EnvironmentData getLoadedData(Class<?> testClass) {
        if (loadedData == null) {
            Properties properties = loadLocalConfig(testClass);
            if (properties == null) {
                loadedData = EnvironmentData.DEFAULT;
            } else {
                loadedData = new EnvironmentData(
                        readPropertyFallback(PROTOCOL_PROPERTIES_KEY, properties, EnvironmentData.DEFAULT.protocol()), readPropertyFallback(HOST_PROPERTIES_KEY, properties, EnvironmentData.DEFAULT.host()), readPropertyFallback(PORT_PROPERTIES_KEY, properties, EnvironmentData.DEFAULT.port()),
                        readPropertyFallback(CONTEXT_PATH_PROPERTIES_KEY, properties, EnvironmentData.DEFAULT.contextPath()),
                        properties);
            }
        }
        return loadedData;
    }

    private String readPropertyFallback(String HOST_PROPERTIES_KEY, Properties properties, String DEFAULT) {
        if (System.getProperties().containsKey(HOST_PROPERTIES_KEY)) {
            return System.getProperties().getProperty(HOST_PROPERTIES_KEY);
        }
        if (properties.containsKey(HOST_PROPERTIES_KEY)) {
            return properties.getProperty(HOST_PROPERTIES_KEY);
        }
        return DEFAULT;
    }

    private Properties loadLocalConfig(Class<?> testClass) {
        Properties loadedProperties = loadLocalConfigFromClasspath(testClass);
        if (loadedProperties == null) {
            return loadLocalConfigFromFile();
        }
        return loadedProperties;
    }

    private Properties loadLocalConfigFromFile() {
        File dataFile = new File("src/test/resources/localconfig.properties");
        if (dataFile.exists()) {
            try (InputStream fileStream = new FileInputStream(dataFile)) {
                Properties properties = new Properties();
                properties.load(fileStream);
                return properties;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    private Properties loadLocalConfigFromClasspath(Class<?> testClass) {
        ClassLoader classLoader = testClass.getClassLoader();
        var classpathResource = classLoader.getResource("localconfig.properties");
        if (classpathResource != null) {
            try (InputStream classpathStream = classpathResource.openStream()) {
                if (classpathStream != null) {
                    Properties properties = new Properties();
                    properties.load(classpathStream);
                    return properties;
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        ExtensionContext.Store store = getStore(extensionContext);
        if (store.get(STORE_BEFORE_ALL_KEY) == null) {
            store.put(STORE_BEFORE_ALL_KEY, getLoadedData(extensionContext.getRequiredTestClass()));
        }
    }

    private static ExtensionContext.Store getStore(ExtensionContext extensionContext) {
        return extensionContext.getStore(ExtensionContext.Namespace.create(extensionContext.getRequiredTestClass()));
    }
}
