package eu.rarogsoftware.commons.test.webdriver.playwright;

import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import eu.rarogsoftware.commons.test.webdriver.AbstractWebdriverTester;
import eu.rarogsoftware.commons.test.webdriver.PageObject;

import java.net.MalformedURLException;
import java.net.URI;

/**
 * Playwright based tester for running tests using page objects pattern.
 */
public class WebdriverTester extends AbstractWebdriverTester {
    private BrowserContext browserContext;
    private Page mainPage;

    public WebdriverTester(BrowserContext browserContext, Page page) {
        this.browserContext = browserContext;
        mainPage = page;
        setTimeout(getTimeout());
    }

    @Override
    public void setTimeout(Integer timeout) {
        super.setTimeout(timeout);
        browserContext.setDefaultTimeout(getTimeout());
        browserContext.setDefaultNavigationTimeout(getTimeout());
        mainPage.setDefaultTimeout(getTimeout());
        mainPage.setDefaultNavigationTimeout(getTimeout());
    }

    public WebdriverTester(BrowserContext browserContext) {
        this(browserContext, browserContext.newPage());
    }

    @Override
    public void navigate(URI uri) throws MalformedURLException {
        mainPage.navigate(uri.toString());
    }

    @Override
    public <U extends PageObject> U bindPage(U pageObject) {
        getPageObjectSuperClass();
        var playwrightPageObject = (PlaywrightPageObject) pageObject;
        playwrightPageObject.setTester(this);
        playwrightPageObject.bind(mainPage);
        return pageObject;
    }

    @Override
    protected Class<? extends PageObject> getPageObjectSuperClass() {
        return PlaywrightPageObject.class;
    }

    @Override
    public String getPageSource() {
        return mainPage.content();
    }

    @Override
    public String getTitle() {
        return mainPage.title();
    }

    @Override
    public String getCurrentAbsoluteUrl() {
        return mainPage.url();
    }

    /**
     * Opens page object as new page separated from main page managed by tester. In any other aspect works the same way
     * as {@link eu.rarogsoftware.commons.test.webdriver.WebdriverTester#goTo(Class)}
     * @param pageObjectClass page object type to create
     * @return instance of page object opened as separated page
     */
    public <U extends PlaywrightPageObject> U goToNewPage(Class<U> pageObjectClass) {
        U pageObject = constructPageObject(pageObjectClass);
        return goToNewPage(pageObject);
    }

    /**
     * Opens page object as new page separated from main page managed by tester. In any other aspect works the same way
     * as {@link eu.rarogsoftware.commons.test.webdriver.WebdriverTester#goTo(PageObject)}
     * @param pageObject page object to bind
     * @return instance of page object opened as separated page
     */
    public <U extends PlaywrightPageObject> U goToNewPage(U pageObject) {
        var pageUrl = pageObject.getTargetPageUrl();
        var targetUrl = pageUrl.startsWith("/") ? pageUrl.substring(1) : pageUrl;
        var newPage = browserContext.newPage();
        newPage.navigate(targetUrl);
        bindPage(pageObject, newPage);
        return pageObject;
    }

    /**
     * Bind page object to specified {@link Page} object. In any other aspect works the same way
     * as {@link eu.rarogsoftware.commons.test.webdriver.WebdriverTester#bindPage(PageObject)}
     * @param pageObjectClass page object class to create
     * @param page page with already opened page
     * @return instance of page object opened as separated page
     */
    public <U extends PlaywrightPageObject> U bindPage(Class<U> pageObjectClass, Page page) {
        U pageObject = constructPageObject(pageObjectClass);
        return bindPage(pageObject, page);
    }


    /**
     * Bind page object to specified {@link Page} object. In any other aspect works the same way
     * as {@link eu.rarogsoftware.commons.test.webdriver.WebdriverTester#bindPage(PageObject)}
     * @param pageObject page object to bind
     * @param page page with already opened page
     * @return instance of page object opened as separated page
     */
    public <U extends PlaywrightPageObject> U bindPage(U pageObject, Page page) {
        pageObject.bind(page);
        return pageObject;
    }
}
