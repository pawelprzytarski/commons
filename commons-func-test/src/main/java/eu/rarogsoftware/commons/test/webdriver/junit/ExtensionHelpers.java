package eu.rarogsoftware.commons.test.webdriver.junit;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.commons.test.webdriver.selenium.WebdriverTester;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.Field;

import static eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension.STORE_BEFORE_ALL_KEY;
import static eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension.STORE_BEFORE_EACH_KEY;

public final class ExtensionHelpers {
    private static final Logger logger = LoggerFactory.getLogger(ExtensionHelpers.class);
    private ExtensionHelpers() {
    }

    public static EnvironmentData getEnvironmentData(ExtensionContext context) {
        var store = getStore(context);
        var environmentData = store.get(STORE_BEFORE_EACH_KEY, EnvironmentData.class);
        if (environmentData == null) {
            environmentData = store.get(STORE_BEFORE_ALL_KEY, EnvironmentData.class);
        }
        if (environmentData == null) {
            throw new IllegalStateException("EnvironmentalData are not available");
        }
        return environmentData;
    }

    public static ExtensionContext.Store getStore(ExtensionContext extensionContext) {
        return extensionContext.getStore(ExtensionContext.Namespace.create(extensionContext.getRequiredTestClass()));
    }
    public static <T> void setFieldValueIfExists(Object o, Class<T> expectedFieldClass, T value) throws IllegalAccessException {
        var field = getDataField(o, expectedFieldClass);
        if (field == null) {
            return;
        }
        field.set(o, value);
    }

    public static <T> void setFieldValue(Object o, Class<T> expectedFieldClass, T value) throws IllegalAccessException {
        var field = getDataField(o, expectedFieldClass);
        if (field == null) {
            logger.warn("Class {} don't have accessible field of type {}. Extension cannot populate required fields", o.getClass(), WebdriverTester.class.getCanonicalName());
            return;
        }
        field.set(o, value);
    }

    public static <T> T getFieldValue(Object o, Class<T> expectedFieldClass) throws IllegalAccessException {
        var field = getDataField(o, expectedFieldClass);
        if (field == null) {
            logger.warn("Class {} don't have accessible field of type {}. Extension cannot populate required fields", o.getClass(), WebdriverTester.class.getCanonicalName());
            return null;
        }
        return (T) field.get(o);
    }

    public static <T> Field getDataField(Object o, Class<T> expectedFieldClass) {
        return getDataField(o, o.getClass(), expectedFieldClass);
    }

    static <T> Field getDataField(Object o, Class<?> clazz, Class<T> expectedFieldClass) {
        if (clazz.equals(Object.class)) {
            return null;
        }
        Field dataField = getDataField(o, clazz.getSuperclass(), expectedFieldClass);
        for (Field field : clazz.getDeclaredFields()) {
            if (expectedFieldClass.isAssignableFrom(field.getType())
                    && (field.isAnnotationPresent(Inject.class))) {
                if (!field.canAccess(o)) {
                    field.setAccessible(true);
                }
                dataField = field;
            }
        }
        return dataField;
    }
}
