package eu.rarogsoftware.commons.test.webdriver.playwright;

import java.lang.annotation.*;

/**
 * Settings for playwright initiated with {@link eu.rarogsoftware.commons.test.webdriver.junit.PlaywrightTesterExtension}.
 * @since 0.1.0
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PlaywrightSettings {
    /**
     * Defines browser to use for test(s).
     * If defined on class level it sets default for all tests.
     * If defined on method level it sets browser for this test only.
     * NOTE: tests with other browser than class browser always starts in new context.
     */
    BrowserType browser() default BrowserType.FIREFOX;

    /**
     * Defines if browser context should be shared between all tests in class or should be cleared for each test.
     * If defined on method level only {link {@link BrowserContextScope#METHOD}} has effect.
     */
    BrowserContextScope contextCreation() default BrowserContextScope.TEST_INSTANCE;

    enum BrowserContextScope {
        TEST_INSTANCE,
        METHOD
    }
}
