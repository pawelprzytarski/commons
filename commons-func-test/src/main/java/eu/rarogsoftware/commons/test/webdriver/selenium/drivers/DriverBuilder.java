package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import org.openqa.selenium.WebDriver;

import java.io.File;

public interface DriverBuilder {
    void setBrowserDir(File browserDir);

    void setProfileDir(File browserProfile);

    WebDriver build();
}
