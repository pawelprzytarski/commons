package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import eu.rarogsoftware.commons.test.webdriver.OsType;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.function.Function;

public class EmbeddedDriverLoader implements SeleniumDriverLoader {
    private static final Logger logger = LoggerFactory.getLogger(EmbeddedDriverLoader.class);
    private static final String PACKAGE_MANIFEST = "profile.package";
    private static final String BROWSER_MANIFEST = "browser.package";
    private final BrowserType browserType;
    private final File webdriverDirFile;
    private final DriverBuilder driverBuilder;

    public EmbeddedDriverLoader(File webdriverDir, BrowserType browserType, Function<OsType, DriverBuilder> driverBuilder) {
        this.webdriverDirFile = webdriverDir.getAbsoluteFile();
        this.browserType = browserType;
        this.driverBuilder = driverBuilder.apply(browserType.getOs());
    }

    @Override
    public void validatePrerequisites() {
        if (!webdriverDirFile.exists()) {
            if (!webdriverDirFile.mkdirs()) {
                throw new IllegalStateException("Cannot create webdrivers directory");
            }
        } else {
            if (!webdriverDirFile.isDirectory()) {
                throw new IllegalStateException("Path to webdriver directory is not a directory");
            }
        }
    }

    @Override
    public void downloadDriver() {
        String browserName = browserType.getBrowser();
        String osDirName = browserType.getOs().getSystemPath();
        String profileName = browserName + "-profile";

        final String browserResource = "/" + browserName + "-" + osDirName + ".zip";
        final String profileResource = "/" + profileName + ".zip";
        final String osProfileResource = "/" + profileName + "-" + osDirName + ".zip";

        try {
            File browserDir = extractBrowser(webdriverDirFile, browserResource);
            File browserProfile = extractProfile(webdriverDirFile, profileResource, osProfileResource);
            driverBuilder.setBrowserDir(browserDir);
            driverBuilder.setProfileDir(browserProfile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private File extractProfile(File destination, String profileResource, String osProfileResource) throws IOException {
        if (resourceExists(osProfileResource)) {
            return extractZipProfileAndLog(destination, osProfileResource);
        } else if (resourceExists(profileResource)) {
            return extractZipProfileAndLog(destination, profileResource);
        } else {
            logger.info("Embedded profile for {}:{} is not attached", browserType.getBrowser(), browserType.getOs());
            return null;
        }
    }

    private static boolean resourceExists(String path) {
        return EmbeddedDriverLoader.class.getResource(path) != null;
    }

    private File extractZipProfileAndLog(File destination, String osProfileResource) throws IOException {
        final File profileDir = extractZip(destination, osProfileResource);
        if (logger.isInfoEnabled()) {
            logger.info("Installed profile {}: {}", profileDir, readManifestInfo(new File(profileDir, PACKAGE_MANIFEST)));
        }
        return profileDir;
    }

    public static File extractZip(File destDir, String internalPath) throws IOException {
        InputStream internalStream = null;

        File targetDir = new File(destDir, internalPath.substring(internalPath.lastIndexOf('/'), internalPath.length() - ".zip".length()));
        if (targetDir.exists()) {
            return targetDir;
        }
        try {
            logger.info("unzipping {}", internalPath);
            targetDir.mkdirs();

            internalStream = EmbeddedDriverLoader.class.getResourceAsStream(internalPath);
            if (internalStream == null) {
                throw new IOException("Zip file not found: " + internalPath);
            }

            final File tempFile = File.createTempFile("firefox", "zip");
            try (
                    BufferedInputStream bis = new BufferedInputStream(internalStream)
            ) {
                Files.copy(bis, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }

            try (
                    ZipFile zip = new ZipFile(tempFile)
            ) {
                final var iterator = zip.getEntries();
                while (iterator.hasMoreElements()) {
                    var entry = iterator.nextElement();
                    if (entry.getName().endsWith("/")) {
                        new File(targetDir, entry.getName()).mkdirs();
                    } else {
                        File file = new File(targetDir, entry.getName());
                        try (
                                FileOutputStream fos = new FileOutputStream(file);
                                InputStream is = zip.getInputStream(entry);
                                BufferedInputStream bis = new BufferedInputStream(is)
                        ) {
                            IOUtils.copy(bis, fos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if ((entry.getUnixMode() & 0111) > 0) {
                            file.setExecutable(true);
                        }
                    }
                }
            }
        } finally {
            IOUtils.closeQuietly(internalStream);
        }
        return targetDir;
    }

    private File extractBrowser(File destination, String browserResource) throws IOException {
        if (!resourceExists(browserResource)) {
            logger.info("Browser {}:{} is not supported", browserType.getBrowser(), browserType.getOs());
            return null;
        }
        final File browserDir = extractZip(destination, browserResource);

        logger.info("Installed browser {}: {}", browserDir, readManifestInfo(new File(browserDir, BROWSER_MANIFEST)));
        return browserDir;
    }

    private static String readManifestInfo(File manifestFile) throws IOException {
        if (manifestFile.exists()) {
            String contents = FileUtils.readFileToString(manifestFile, StandardCharsets.UTF_8);
            return contents.trim().replaceAll("\n", ", ");
        } else {
            return "Manifest file not found " + manifestFile;
        }
    }

    @Override
    public WebDriver initializeDriver() {
        return driverBuilder.build();
    }
}
