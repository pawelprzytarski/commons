package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import eu.rarogsoftware.commons.test.webdriver.OsType;

enum BrowserType {
    FIREFOX_LINUX("firefox", OsType.LINUX64),
    FIREFOX_WINDOWS("firefox", OsType.WINDOWS);

    private final String browser;
    private final OsType os;

    BrowserType(String browser, OsType os) {
        this.browser = browser;
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public OsType getOs() {
        return os;
    }
}
