package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import eu.rarogsoftware.commons.test.webdriver.XvfbRunner;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.EnumMap;
import java.util.Locale;
import java.util.function.Function;

public class SeleniumDriverFactory {
    private static final String SELENIUM_DRIVER_TYPE_PROPERTY = "selenium.driver.type";
    private final Logger logger = LoggerFactory.getLogger(SeleniumDriverFactory.class);
    private final EnumMap<SupportedDriverType, SeleniumDriverLoader> driverLoaderMap = new EnumMap<>(SupportedDriverType.class);
    private final EnumMap<SupportedDriverType, WebDriver> driverMap = new EnumMap<>(SupportedDriverType.class);
    private final XvfbRunner xvfbRunner = new XvfbRunner(new File("target/webdriver/xvfb"));
    private static SeleniumDriverFactory instance;
    private String webdriverDirectory = "target/webdriver";

    public enum SupportedDriverType {
        EmbeddedFirefox,
        Firefox,
        Chromium
    }

    public static SeleniumDriverFactory getInstance() {
        if (instance == null) {
            instance = new SeleniumDriverFactory();
        }
        return instance;
    }

    public void setWebdriverDirectory(String webdriverDirectory) {
        this.webdriverDirectory = webdriverDirectory;
        xvfbRunner.setXvfbBaseDir(new File(webdriverDirectory, "xvfb"));
    }

    public WebDriver getDriver() {
        SupportedDriverType driverType = detectDriverType();
        return getDriver(driverType);
    }

    private SupportedDriverType detectDriverType() {
        SupportedDriverType driverType = null;
        String driverTypeProperty = System.getProperty(SELENIUM_DRIVER_TYPE_PROPERTY);
        if (driverTypeProperty == null) {
            logger.info("Using default driver. To use different driver set system property: selenium.driver.type");
            driverType = SupportedDriverType.EmbeddedFirefox;
        } else {
            for (SupportedDriverType value : SupportedDriverType.values()) {
                if (value.name().toLowerCase(Locale.ROOT).equals(driverTypeProperty.toLowerCase(Locale.ROOT))) {
                    driverType = value;
                    break;
                }
            }
            if (driverType == null) {
                logger.warn("Cannot recognize driver specified by selenium.driver.type property");
                driverType = SupportedDriverType.EmbeddedFirefox;
            }
        }
        return driverType;
    }

    public WebDriver getDriver(SupportedDriverType driverType) {
        Function<SupportedDriverType, WebDriver> factory = (type) -> {
            SeleniumDriverLoader driverLoader = getDriverFromType(type);
            if (isXvfbRequired() && XvfbRunner.isSupportedOS()) {
                if (!xvfbRunner.isStarted()) {
                    xvfbRunner.start();
                    Runtime.getRuntime().addShutdownHook(new Thread(xvfbRunner::stop));
                }
                System.setProperty("DISPLAY", xvfbRunner.getDisplay());
            }
            WebDriver webDriver = driverLoader.initializeDriver();
            Runtime.getRuntime().addShutdownHook(new Thread(webDriver::quit));
            return webDriver;
        };

        if (isBrowseReuseEnabled()) {
            return driverMap.computeIfAbsent(driverType, factory);
        } else {
            return factory.apply(driverType);
        }
    }

    private boolean isXvfbRequired() {
        return Boolean.parseBoolean(System.getProperty("xvfb.enable", "false"));
    }

    private boolean isBrowseReuseEnabled() {
        return Boolean.parseBoolean(System.getProperty("selenium.reuse.browser", "true"));
    }

    private SeleniumDriverLoader getDriverFromType(SupportedDriverType driverType) {
        return driverLoaderMap.computeIfAbsent(driverType, (type) -> {
            SeleniumDriverLoader driverLoader = switch (type) {
                case EmbeddedFirefox -> new EmbeddedDriverLoader(new File(webdriverDirectory), getFirefoxType(), FirefoxDriverBuilder::new);
                case Firefox -> new LocalFirefoxDriverLoader();
                case Chromium -> new LocalChromiumDriverLoader();
            };
            driverLoader.validatePrerequisites();
            driverLoader.downloadDriver();
            return driverLoader;
        });
    }

    private BrowserType getFirefoxType() {
        BrowserType browserType = BrowserType.FIREFOX_LINUX;
        if (SystemUtils.IS_OS_WINDOWS) {
            browserType = BrowserType.FIREFOX_WINDOWS;
        }
        return browserType;
    }
}
