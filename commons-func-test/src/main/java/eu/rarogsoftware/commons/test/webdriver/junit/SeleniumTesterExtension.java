package eu.rarogsoftware.commons.test.webdriver.junit;

import eu.rarogsoftware.commons.test.webdriver.selenium.WebdriverTester;
import eu.rarogsoftware.commons.test.webdriver.selenium.drivers.SeleniumTesterFactory;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

/**
 * Downloads and initialize browser for selenium webdriver, initializes selenium webdriver for downloaded browser
 * and injects tests object with {@link WebdriverTester}. Makes using selenium webdriver an easy task.
 */
public class SeleniumTesterExtension implements TestInstancePostProcessor, BeforeEachCallback, AfterTestExecutionCallback {
    private static final Pattern PATTERN = Pattern.compile("[^A-Za-z0-9_\\-]");
    private final Logger logger = LoggerFactory.getLogger(SeleniumTesterExtension.class);
    private final SeleniumTesterFactory testerFactory = new SeleniumTesterFactory();

    @Override
    public void postProcessTestInstance(Object o, ExtensionContext extensionContext) throws Exception {
        ExtensionHelpers.setFieldValue(o, WebdriverTester.class, testerFactory.createSeleniumTester());
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        if (extensionContext.getTestInstance().isPresent()) {
            Object o = extensionContext.getTestInstance().get();
            var tester = ExtensionHelpers.getFieldValue(o, WebdriverTester.class);
            if (tester == null) {
                return;
            }
            if (tester != null) {
                tester.setEnvironmentData(ExtensionHelpers.getEnvironmentData(extensionContext));
                tester.getWebdriverConfig().reinitialize();
            }
        }
    }

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        if (extensionContext.getTestInstance().isPresent()) {
            Object o = extensionContext.getTestInstance().get();
            var tester = ExtensionHelpers.getFieldValue(o, WebdriverTester.class);
            if (tester == null) {
                return;
            }
            if (tester != null) {
                makeScreenshotIfFailure(extensionContext, tester);
                tester.getWebdriverConfig().clean();
            }
        }
    }

    private void makeScreenshotIfFailure(ExtensionContext extensionContext, WebdriverTester tester) {
        if (extensionContext.getExecutionException().isPresent()) {
            String screenshotDirectory = System.getProperty("selenium.screenshot.directory", "target/reports/screenshots/");
            String className = sanitizeString(extensionContext.getTestClass().map(Class::getName).orElse(""));
            String testName = sanitizeString(extensionContext.getDisplayName());
            String date = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")
                    .withZone(ZoneId.of("UTC"))
                    .format(Instant.now());
            Path saveScreenshotFile = Path.of(screenshotDirectory, className, testName + date + ".png");
            Path saveHtmlFile = Path.of(screenshotDirectory, className, testName + date + ".html");
            logger.info("Test failed. Current url: `{}` . Saving screenshot as {}", tester.getCurrentAbsoluteUrl(), saveScreenshotFile.toAbsolutePath());
            logger.info("Test failed. Current url: `{}` . Saving page content as {}", tester.getCurrentAbsoluteUrl(), saveHtmlFile.toAbsolutePath());

            File screenshot = ((TakesScreenshot) tester.getWebDriver()).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(screenshot, saveScreenshotFile.toFile());
            } catch (IOException e) {
                logger.error("Failed to save screenshot to selected location", e);
            }
            try {
                FileUtils.write(saveHtmlFile.toFile(), tester.getPageSource(), StandardCharsets.UTF_8);
            } catch (IOException e) {
                logger.error("Failed to save screenshot to selected location", e);
            }
        }
    }

    private String sanitizeString(String string) {
        return PATTERN.matcher(string).replaceAll("_");
    }
}
