package eu.rarogsoftware.commons.test.webdriver;

public enum OsType {
    LINUX64("linux64"),
    WINDOWS("windows");

    private final String systemPath;

    OsType(String systemPath) {
        this.systemPath = systemPath;
    }

    public String getSystemPath() {
        return systemPath;
    }
}
