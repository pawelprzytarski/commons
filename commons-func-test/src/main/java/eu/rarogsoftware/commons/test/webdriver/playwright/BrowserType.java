package eu.rarogsoftware.commons.test.webdriver.playwright;

public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome"),
    WEBKIT("webkit");


    private final String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
