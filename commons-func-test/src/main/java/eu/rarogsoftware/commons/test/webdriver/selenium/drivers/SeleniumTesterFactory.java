package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import eu.rarogsoftware.commons.test.webdriver.selenium.WebdriverConfig;
import eu.rarogsoftware.commons.test.webdriver.selenium.WebdriverTester;

public class SeleniumTesterFactory {
    public WebdriverTester createSeleniumTester() {
        WebdriverConfig seleniumConfig = getSeleniumConfig();
        return new WebdriverTester(seleniumConfig);
    }

    public WebdriverConfig getSeleniumConfig() {
        return new WebdriverConfig();
    }
}
