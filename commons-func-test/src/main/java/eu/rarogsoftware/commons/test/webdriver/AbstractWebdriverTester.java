package eu.rarogsoftware.commons.test.webdriver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

public abstract class AbstractWebdriverTester implements WebdriverTester {
    protected Integer timeout = 10000;
    private EnvironmentData environmentData = EnvironmentData.DEFAULT;

    @Override
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    @Override
    public int getTimeout() {
        return timeout;
    }

    @Override
    public void goTo(String url) {
        URI resolvedUrl;
        try {
            resolvedUrl = new URI(url);
            if (!resolvedUrl.isAbsolute()) {
                if (environmentData == null) {
                    throw new IllegalArgumentException("Relative URLs are unsupported if environmental data is not set");
                }
                if (url.startsWith("/")) {
                    url = url.substring(1);
                }
                resolvedUrl = new URI(environmentData.baseUrl() + url);
            }
            navigate(resolvedUrl);
        } catch (URISyntaxException | MalformedURLException e) {
            throw new IllegalArgumentException("Provided url is incorrect", e);
        }
    }

    @Override
    public <U extends PageObject> U goTo(Class<U> pageObjectClass) {
        U pageObject = constructPageObject(pageObjectClass);
        return goTo(pageObject);
    }

    @Override
    public <U extends PageObject> U goTo(U pageObject) {
        assertPageObjectHasCorrectType(pageObject.getClass());
        String pageUrl = pageObject.getTargetPageUrl();
        // because localhost/endpoint and localhost//endpoint are different endpoints
        goTo(pageUrl.startsWith("/") ? pageUrl.substring(1) : pageUrl);
        bindPage(pageObject);
        return pageObject;
    }

    @Override
    public <U extends PageObject> U bindPage(Class<U> pageObjectClass) {
        U pageObject = constructPageObject(pageObjectClass);
        return bindPage(pageObject);
    }

    protected  <U extends PageObject> U constructPageObject(Class<U> pageObjectClass) {
        assertPageObjectHasCorrectType(pageObjectClass);
        Constructor<U> pageObjectConstructor = null;
        try {
            pageObjectConstructor = pageObjectClass.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Page object must have constructor without parameters", e);
        }
        if ((pageObjectConstructor.getModifiers() & Modifier.PUBLIC) == 0) {
            throw new IllegalArgumentException("Page object's constructor must be public");
        }
        try {
            return pageObjectConstructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("Page object cannot be created", e);
        }
    }

    private <U extends PageObject> void assertPageObjectHasCorrectType(Class<U> pageObjectClass) {
        if(!getPageObjectSuperClass().isAssignableFrom(pageObjectClass)){
            throw new IllegalArgumentException("This tester is not working with page object of different type");
        }
    }

    protected abstract Class<? extends PageObject> getPageObjectSuperClass();

    @Override
    public String getCurrentUrl() {
        if (environmentData == null) {
            return getCurrentAbsoluteUrl();
        } else {
            String currentUrl = getCurrentAbsoluteUrl();
            String baseUrl = environmentData.baseUrl();
            if (currentUrl.startsWith(baseUrl)) {
                return currentUrl.substring(baseUrl.length() - 1);
            } else {
                return currentUrl;
            }
        }
    }

    @Override
    public void setEnvironmentData(EnvironmentData environmentData) {
        this.environmentData = environmentData;
    }

    @Override
    public EnvironmentData getEnvironmentData() {
        return environmentData;
    }
}
