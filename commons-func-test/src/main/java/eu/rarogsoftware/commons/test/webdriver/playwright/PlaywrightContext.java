package eu.rarogsoftware.commons.test.webdriver.playwright;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Playwright;

import java.util.Map;
import java.util.Objects;

public final class PlaywrightContext implements AutoCloseable {
    private final Playwright playwright;
    private final Map<String, Browser> browsers;
    private final Browser defaultBrowser;
    private Browser testBrowser;

    public PlaywrightContext(Playwright playwright, Map<String, Browser> browsers, Browser defaultBrowser, Browser testBrowser) {
        this.playwright = playwright;
        this.browsers = browsers;
        this.defaultBrowser = defaultBrowser;
        this.testBrowser = testBrowser;
    }

    public PlaywrightContext(Playwright playwright, Map<String, Browser> browsers, Browser defaultBrowser) {
        this(playwright, browsers, defaultBrowser, defaultBrowser);
    }

    @Override
    public void close() throws Exception {
        browsers.forEach((s, browser) -> browser.close());
        playwright.close();
    }

    public Playwright playwright() {
        return playwright;
    }

    public Map<String, Browser> browsers() {
        return browsers;
    }

    public Browser defaultBrowser() {
        return defaultBrowser;
    }

    public Browser testBrowser() {
        return testBrowser;
    }

    public void changeTestBrowser(String name) {
        testBrowser = browsers.get(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (PlaywrightContext) obj;
        return Objects.equals(this.playwright, that.playwright) &&
                Objects.equals(this.browsers, that.browsers) &&
                Objects.equals(this.defaultBrowser, that.defaultBrowser) &&
                Objects.equals(this.testBrowser, that.testBrowser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playwright, browsers, defaultBrowser, testBrowser);
    }

    @Override
    public String toString() {
        return "PlaywrightContext[" +
                "playwright=" + playwright + ", " +
                "browsers=" + browsers + ", " +
                "defaultBrowser=" + defaultBrowser + ", " +
                "testBrowser=" + testBrowser + ']';
    }

}
