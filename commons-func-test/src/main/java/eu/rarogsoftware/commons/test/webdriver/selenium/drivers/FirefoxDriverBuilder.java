package eu.rarogsoftware.commons.test.webdriver.selenium.drivers;

import com.google.common.collect.ImmutableMap;
import eu.rarogsoftware.commons.test.webdriver.OsType;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FirefoxDriverBuilder implements DriverBuilder{
    private static final Logger logger = LoggerFactory.getLogger(FirefoxDriverBuilder.class);
    private static final String FIREFOX_WEBDRIVER_LOGFILE = "webdriver.firefox.logfile";
    private static final String DRIVER_GECKODRIVER_FILENAME = "geckodriver";
    private final GeckoDriverService.Builder builder = new GeckoDriverService.Builder();
    private final OsType osType;
    private FirefoxProfile profile;

    public FirefoxDriverBuilder(OsType osType) {
        this.osType = osType;
    }

    @Override
    public void setBrowserDir(File browserDir) {
        String binaryPath = getBinaryPath();
        File browserBinary = getBrowserBinary(binaryPath, browserDir);
        builder.usingFirefoxBinary(new FirefoxBinary(browserBinary));
        if (!browserBinary.setExecutable(true, false)) {
            logger.warn("Cannot make browser binary executable");
        }
    }

    private String getBinaryPath() {
        return switch (osType){
            case LINUX64 -> "firefox-bin";
            case WINDOWS -> "firefox.exe";
        };
    }

    private String getGeckodriverFilename() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return DRIVER_GECKODRIVER_FILENAME + ".exe";
        }
        return DRIVER_GECKODRIVER_FILENAME;
    }

    private File getBrowserBinary(String binaryPath, File browserDir) {
        if (browserDir == null) {
            return null;
        }
        return new File(browserDir, binaryPath);
    }

    @Override
    public void setProfileDir(File browserProfile) {
        profile = new FirefoxProfile(browserProfile);
        addPreferencesToProfile(profile, browserProfile);
        File driverBinary = new File(browserProfile.getAbsolutePath(), getGeckodriverFilename());
        builder.usingDriverExecutable(driverBinary);
        if (!driverBinary.setExecutable(true, false)) {
            logger.warn("Cannot make geckodriver executable");
        }
    }

    @Override
    public WebDriver build() {
        setSystemProperties(builder);
        FirefoxOptions options = new FirefoxOptions();
        if(Boolean.getBoolean("selenium.headless.enabled")){
            options.setHeadless(true);
        }
        if (!Boolean.getBoolean("selenium.info.logs.disabled")) {
            options.setLogLevel(FirefoxDriverLogLevel.INFO);
        }else{
            options.setLogLevel(FirefoxDriverLogLevel.ERROR);
        }
        if (Boolean.getBoolean("selenium.debug.logs.enabled")) {
            options.setLogLevel(FirefoxDriverLogLevel.DEBUG);
        }
        options.setLogLevel(FirefoxDriverLogLevel.DEBUG);
        options.setProfile(profile);

        return new FirefoxDriver(builder.build(), options);
    }

    private static void addPreferencesToProfile(FirefoxProfile profile, File profilePath)
    {
        File profilePreferencesFile = new File(profilePath, "profile.preferences");

        if (profilePreferencesFile.exists())
        {
            Properties properties = new Properties();
            try(InputStream is = new FileInputStream(profilePreferencesFile)) {
                properties.load(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
            properties.keySet().forEach(key ->
            {
                Object value = properties.get(key);
                profile.setPreference(key.toString(), value);
            });
        }
    }

    private static void setSystemProperties(GeckoDriverService.Builder builder)
    {
        System.out.println("Display: " + System.getProperty("DISPLAY"));
        if (System.getProperty("DISPLAY") != null)
        {
            builder.withEnvironment(ImmutableMap.of("DISPLAY", System.getProperty("DISPLAY")));
        }
        new File("target/reports/webdriver/").mkdirs();
        System.setProperty(FIREFOX_WEBDRIVER_LOGFILE, new File("target/reports/webdriver/firefox-test.log").getAbsolutePath());
    }
}
