package eu.rarogsoftware.commons.test.webdriver;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class XvfbRunner {
    private static Logger logger = LoggerFactory.getLogger(XvfbRunner.class);
    private static final int DEFAULT_DISPLAY_NUMBER = 20;
    private static final int MAX_PORT_TRIES = 10;
    private String display = System.getProperty("xvfb.display", ":" + DEFAULT_DISPLAY_NUMBER);
    private boolean started = false;
    private File xvfbBaseDir;
    private Process xfvbProcess;

    public XvfbRunner(File xvfbBaseDir) {
        this.xvfbBaseDir = xvfbBaseDir;
    }

    public void setXvfbBaseDir(File xvfbBaseDir) {
        this.xvfbBaseDir = xvfbBaseDir;
    }

    public static boolean isSupportedOS() {
        return SystemUtils.IS_OS_UNIX;
    }

    public boolean isStarted() {
        return started;
    }

    public String getDisplay() {
        return display;
    }

    public void start() {
        final List<String> optionsList = parseXvfbOptions();

        logger.info("Starting xvfb server");
        display = detectUsableDisplay();
        logger.info("Exporting display on port: {}", display);
        logger.info("Using options: {}", optionsList);

        List<String> cmd = new ArrayList<>();
        cmd.add(getXvfbExecutableName());
        cmd.add(display);
        cmd.addAll(optionsList);

        writeDisplayProperties();
        xfvbProcess = ProcessUtils.runProcessInBackground(new ProcessBuilder(cmd));
        logger.info("Waiting till xvfb is up.");
        Instant waitUntil = Instant.now().plus(60, ChronoUnit.SECONDS);
        while (isXvfbNotStartedOnPort(display) && Instant.now().isBefore(waitUntil)) {
            try {
                System.out.println('.');
                System.out.flush();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // ignore
            }
        }
        if (isXvfbNotStartedOnPort(display)) {
            stop();
            throw new RuntimeException("Xvfb do not starts under 60 seconds");
        }
        logger.info("xvfb is up!");
        started = true;
    }

    private String getXvfbExecutableName() {
        return System.getProperty("xvfb.executable", "Xvfb");
    }

    private List<String> parseXvfbOptions() {
        if (System.getProperty("xvfb.options") != null) {
            final List<String> optionsList = Arrays.asList(System.getProperty("xvfb.options").split(","));
            if (Collections.indexOfSubList(optionsList, Arrays.asList("-listen", "tcp")) == -1) {
                System.out.println("WARNING: '-listen,tcp' not set. Xvfb might hang without this " +
                        "option. Recommend adding to xvfb.options.");
            }
            return optionsList;
        } else {
            return Arrays.asList("-once", "-listen", "tcp");
        }
    }

    public void stop() {
        if (xfvbProcess != null) {
            xfvbProcess.destroy();
        }
        started = false;
    }

    private void writeDisplayProperties() {
        try {
            String text = "DISPLAY='" + display + "'\n";
            File propFile = new File(xvfbBaseDir, "xvfb.env.properties");
            FileUtils.writeStringToFile(propFile, text, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save display properties", e);
        }
    }

    private String detectUsableDisplay() {
        int startPort = displayPortToInt(display);
        for (int port = startPort; port < startPort + MAX_PORT_TRIES; port++) {
            String portString = ":" + port;
            if (isXvfbNotStartedOnPort(portString)) {
                return portString;
            }
        }
        throw new RuntimeException("Could not find a usable display");
    }

    private int displayPortToInt(String display) {
        return Integer.parseInt(display.substring(display.indexOf(":") + 1));
    }

    private int decodeDisplayPort(String display) {
        // Normally, the first X11 display is on port 6000, the next on port 6001
        return 6000 + Integer.parseInt(display.substring(display.indexOf(":") + 1));
    }

    boolean isXvfbNotStartedOnPort(String display) {
        int port = decodeDisplayPort(display);
        try {
            new Socket("localhost", port);
            return false;
        } catch (Exception e) {
            return true;
        }
    }
}
